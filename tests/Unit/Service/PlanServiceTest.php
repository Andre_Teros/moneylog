<?php
declare(strict_types=1);

namespace Tests\Unit\Service;

use App\Repository\PlanRepository;
use App\Service\PlanService;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;

class PlanServiceTest extends TestCase
{
    /**
     * @var PlanRepository | MockObject
     */
    private $repository;
    /**
     * @var PlanService
     */
    private $planService;

    public function setUp(): void
    {
        $this->repository = $this->getMockBuilder(PlanRepository::class)
            ->disableOriginalConstructor()
            ->getMock();
        $this->planService = new PlanService($this->repository);
    }

    public function test_should_get_plan_from_db(): void
    {
        $userId = 3;
        $year = 2000;

        $this->repository->expects(self::once())
            ->method('find')
            ->with($userId, $year)
            ->willReturn([
                'aim' => '550',
                'month_aims' => '[
                    {
                        "month": 1, "aim": 1000, "current": 1000
                    },
                    {
                        "month": 2, "aim": 1000, "current": 250
                    }
                ]'
            ]);

        $expectedMonthAims = [
            [
                'month' => 1,
                'aim' => 1000,
                'current' => 1000,
            ],
            [
                'month' => 2,
                'aim' => 1000,
                'current' => 250,
            ],
        ];

        $result = $this->planService->getPlan($userId, $year);

        self::assertSame(550, $result['aim']);
        self::assertSame($expectedMonthAims, $result['monthAims']);
    }

    public function test_should_return_default_values_if_no_data_in_db(): void
    {
        $userId = 3;
        $year = 2000;

        $this->repository->expects(self::once())
            ->method('find')
            ->with($userId, $year)
            ->willReturn(false);

        $result = $this->planService->getPlan($userId, $year);

        self::assertSame(0, $result['aim']);
        self::assertSame([], $result['monthAims']);
    }

    public function test_should_get_using_current_year_if_not_specified(): void
    {
        $userId = 3;
        $year = (int)date('Y');

        $this->repository->expects(self::once())
            ->method('find')
            ->with($userId, $year)
            ->willReturn(false);

        $this->planService->getPlan($userId);
    }

    public function test_should_insert_data(): void
    {
        $userId = 3;
        $year = 2000;
        $aim = 500;
        $data = [
            'aim' => 500,
            'monthAims' => [
                [
                    'month' => 1,
                    'current' => 10,
                    'aim' => 20
                ],
                [
                    'month' => 2,
                    'current' => 0,
                    'aim' => 20
                ],
            ]
        ];

        $encodedMonthAims = '[{"month":1,"current":10,"aim":20},{"month":2,"current":0,"aim":20}]';

        $this->repository->expects(self::once())
            ->method('find')
            ->with($userId, $year)
            ->willReturn(false);

        $this->repository->expects(self::once())
            ->method('insert')
            ->with($userId, $year, $aim, $encodedMonthAims)
            ->willReturn(true);

        $this->repository->expects(self::never())
            ->method('update');


        $result = $this->planService->savePlan($data, $userId, $year);

        self::assertTrue($result);
    }

    public function test_should_update_data(): void
    {
        $userId = 3;
        $year = 2000;
        $aim = 500;
        $data = [
            'aim' => 500,
            'monthAims' => [
                [
                    'month' => 1,
                    'current' => 10,
                    'aim' => 20
                ],
                [
                    'month' => 2,
                    'current' => 0,
                    'aim' => 20
                ],
            ]
        ];

        $encodedMonthAims = '[{"month":1,"current":10,"aim":20},{"month":2,"current":0,"aim":20}]';

        $this->repository->expects(self::once())
            ->method('find')
            ->with($userId, $year)
            ->willReturn([]);

        $this->repository->expects(self::never())
            ->method('insert');

        $this->repository->expects(self::once())
            ->method('update')
            ->with($userId, $year, $aim, $encodedMonthAims)
            ->willReturn(true);

        $result = $this->planService->savePlan($data, $userId, $year);

        self::assertTrue($result);
    }

    public function test_should_update_using_current_year_if_not_specified(): void
    {
        $userId = 3;
        $year = (int)date('Y');

        $this->repository->expects(self::once())
            ->method('find')
            ->with($userId, $year)
            ->willReturn(false);

        $this->repository->expects(self::once())
            ->method('insert')
            ->with($userId, $year, null, null)
            ->willReturn(false);

        $this->planService->savePlan([], $userId);
    }
}
