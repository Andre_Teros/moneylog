import React, { forwardRef, isValidElement } from "react";
import PropTypes from 'prop-types';

export const RefInput = forwardRef((props, ref) => {
    const {type, wrapperClass, errorMessage, preInputElement: PreInputElement, ...rest} = props;
    const inputClasses = ['form-control'];

    if (errorMessage !== null) {
        inputClasses.push('is-invalid')
    }

    return (
        <div className={`input-group ${wrapperClass}`}>
            { isValidElement(PreInputElement) && <PreInputElement/> }
            { typeof PreInputElement === 'string' && <span className="col-1 sub">{PreInputElement}</span> }
            <input
                className={inputClasses.join(' ')}
                type={type}
                ref={ref}
                {...rest}
            />
            <div className="invalid-feedback">{errorMessage}</div>
        </div>

    );
});

RefInput.propTypes = {
    type: PropTypes.string.isRequired,
    wrapperClass: PropTypes.string,
    errorMessage: PropTypes.string,
    preInputElement: PropTypes.oneOfType([PropTypes.string, PropTypes.element]),
};

RefInput.defaultProps = {
    type: 'text',
    errorMessage: null,
};
