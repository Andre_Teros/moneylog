import Noty from 'noty';
import 'noty/lib/noty.css';
import 'noty/lib/themes/bootstrap-v4.css';

const options = {
    theme: 'bootstrap-v4',
    type: 'alert',
    text: 'Some notification text',
    timeout: 2500,
};

export const successNoty = (text) => {
    new Noty({...options, type: 'success', text}).show();
}

export const errorNoty = (text) => {
    new Noty({...options, type: 'error', text}).show();
}
