import React, { isValidElement } from "react";
import PropTypes from 'prop-types';

export const Input = (props) => {
    const {type, wrapperClass, errorMessage, preInputElement: PreInputElement, ...rest} = props;
    const inputClasses = ['form-control'];

    if (errorMessage !== null) {
        inputClasses.push('is-invalid')
    }

    return (
        <div className={`input-group ${wrapperClass}`}>
            { isValidElement(PreInputElement) && <PreInputElement/> }
            { typeof PreInputElement === 'string' && <span className="col-1 sub">{PreInputElement}</span> }
            <input
                className={inputClasses.join(' ')}
                type={type}
                {...rest}
            />
            <div className="invalid-feedback">{errorMessage}</div>
        </div>

    );
};

Input.propTypes = {
    type: PropTypes.string.isRequired,
    wrapperClass: PropTypes.string,
    errorMessage: PropTypes.string,
    preInputElement: PropTypes.oneOfType([PropTypes.string, PropTypes.element]),
};

Input.defaultProps = {
    type: 'text',
    errorMessage: null,
};
