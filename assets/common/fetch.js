function fetchJsonData(url, options) {
    return fetch(url, options)
        .then((response) => {
            return new Promise((resolve, reject) => {
                response.json()
                    .then((body) => {
                        resolve({
                            body,
                            status: response.status,
                        });
                    })
                    .catch((error) => {
                        reject({
                            body: {error: error.message},
                            status: response.status,
                        });
                    });
            })
        });
}

const options = {};

export function fetchGet(url) {
    return fetchJsonData(url, options)
}

export function fetchPost(url, body = {}) {
    return fetchJsonData(url, {...options, method: 'POST', body: JSON.stringify(body)});
}
