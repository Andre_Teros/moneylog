import { createStore, combineReducers, applyMiddleware } from "redux";
import thunkMiddleware from "redux-thunk";
import plan from "../plans/planReducer";

export default (initData = {}) => createStore(
    combineReducers({
        plan,
    }),
    initData,
    applyMiddleware(
        thunkMiddleware
    )
);
