import React, { useCallback } from "react";
import PropTypes from 'prop-types';
import { useDispatch, useSelector } from 'react-redux';
import { setCurrentYear } from "../plans/actions";

export const YearsNav = (props) => {
    const { years } = props;

    const dispatch  = useDispatch();
    const chosenYear = useSelector((state) => state.plan.chosenYear);
    const handleOnClick = useCallback((year) => () => dispatch(setCurrentYear(year)), [dispatch]);

    return (
        <div id="years" className="mb-3">
            <div className="dropdown show mx-0">
                <a
                    className="btn btn-secondary dropdown-toggle"
                    href="#"
                    role="button"
                    id="dropdownMenuLink"
                    data-toggle="dropdown"
                    aria-haspopup="true"
                    aria-expanded="false"
                >
                    year ( {chosenYear} ) &nbsp;
                </a>

                <div className="dropdown-menu" aria-labelledby="dropdownMenuLink">
                    {years.map((year) => {
                        let className = 'dropdown-item';

                        if (year === chosenYear) {
                            className += ' disabled';
                        }

                        return (
                            <a
                                key={year}
                                className={className}
                                onClick={year !== chosenYear ? handleOnClick(year) : null}
                            >
                                {year}
                            </a>
                        )
                    })}
                </div>
            </div>
        </div>
    );
};

YearsNav.propTypes = {
    years: PropTypes.array.isRequired,
};
