import { SET_MONTH_AIMS, SET_CURRENT_YEAR } from "./actions";

const initialState = {
    allPlansData: [],
    chosenYear: 0,
};

export default (state = initialState, { type, payload }) => {
    switch (type) {
        case SET_MONTH_AIMS:
            const { plan, overvalue, chosenYear } = payload;

            let needToAdd = true;

            const newState = {
                ...state,
                allPlansData: state.allPlansData
                    .map((yearPlansData) => {
                        if (yearPlansData.year !== chosenYear) {
                            return yearPlansData;
                        }

                        needToAdd = false;

                        return { ...yearPlansData, month_aims: plan, overvalue };
                    })
            }

            if (needToAdd) {
                newState.allPlansData.push({month_aims: plan, overvalue, year: chosenYear})
            }

            return newState

        case SET_CURRENT_YEAR:
            return { ...state, chosenYear: payload }

        default:
            return state;
    }
}
