import React from "react";
import { render } from 'react-dom';
import { Provider } from "react-redux";

import PlansApp from "./PlansApp";
import NavBar from "../layout/Navbar";
import reducers from "../common/reducers";

import 'semantic-ui-css/components/progress.min.css';
import './style.css';

const planAppDom = document.getElementById('main');
const planInitData = JSON.parse(planAppDom.dataset.init);

const navBarDom = document.getElementById('nav-bar');

const store = reducers({
    plan: planInitData,
});

render(
    <Provider store={store}>
        <PlansApp />
    </Provider>,
    planAppDom
);

render(
    <Provider store={store}>
        <NavBar />
    </Provider>,
    navBarDom
);
