import React from "react";
import PropTypes from 'prop-types';
import { Progress } from "semantic-ui-react";

const ProgressBar = ({current, aim}) => {
    if (current === null || aim === null) {
        return (
            <Progress
                percent={100}
                active
                size='large'
                color='olive'
            />
        );
    }

    return (
        <Progress
            value={current}
            total={aim}
            progress='ratio'
            size='large'
            color='olive'
        />
    );
};

ProgressBar.propTypes = {
    current: PropTypes.number,
    aim: PropTypes.number,
};

ProgressBar.defaultProps = {
    current: null,
    aim: null,
};

export { ProgressBar };
