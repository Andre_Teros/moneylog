import React, { Fragment } from "react";
import PropTypes from 'prop-types';
import Loader from 'react-loader-spinner';
import { ControlBar } from './ControlBar';
import { Deposit } from "@andre_teros/deposit";
import { ButtonMoveOverValue } from "./ButtonMoveOverValue";

const ProgressTable = ({monthAims, overvalue, saveMonthAims}) => {
    if (monthAims === null) {
        return (
            <Loader
                type="Grid"
                color="#b5cc18"
                height={60}
                width={60}
            />
        );
    }

    const deposit = new Deposit(monthAims, overvalue);

    return (
        <Fragment>
            <ControlBar monthAims={monthAims} overvalue={overvalue} saveMonthAims={saveMonthAims}/>
            <table className="table table-bordered">
                <tbody>
                <tr>
                    <th>Total: {deposit.getTotalDeposit()}</th>
                </tr>
                {monthAims.map((item) => {
                    const {month, current, aim} = item;

                    return (
                        <tr key={month}>
                            <td>
                                {month}: {current} / {aim}
                            </td>
                        </tr>
                    );
                })}

                <tr>
                    <td>
                        Overvalue: {deposit.overValue}
                        <ButtonMoveOverValue overvalue={deposit.overValue} saveMonthAims={saveMonthAims}/>
                    </td>
                </tr>

                </tbody>
            </table>
        </Fragment>
    );
}

ProgressTable.propTypes = {
    monthAims: PropTypes.array,
    overvalue: PropTypes.number,
    saveMonthAims: PropTypes.func,
};

ProgressTable.defaultProps = {
    monthAims: null,
};

export { ProgressTable };
