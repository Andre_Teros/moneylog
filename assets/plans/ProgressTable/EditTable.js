import React, { useRef } from "react";
import { RefInput } from "../../common/RefInput";
import PropTypes from "prop-types";
import { useSelector } from 'react-redux';

const months = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12];

const getValueByMonthFactory = (monthAims) => (monthNumber, field) => {
    const monthData = monthAims.find((item) => item.month === monthNumber);

    return monthData === undefined ? 0 : monthData[field];
}

export const EditTable = ({ monthAims, saveMonthAims, overvalue }) => {
    const getValueByMonth = getValueByMonthFactory(monthAims);

    const superRef = useRef(null);
    const chosenYear = useSelector((state) => state.plan.chosenYear);

    const handleOnclick = () => {
        const inputList = superRef.current.getElementsByTagName('input');

        const newMonthAims = [];

        for (let input of inputList) {
            let month = Number(input.dataset.month);
            let aim = Number(input.value);

            newMonthAims.push({
                month,
                current: getValueByMonth(month, 'current'),
                aim,
            });
        }

        saveMonthAims(newMonthAims, overvalue, chosenYear);
    }

    const handleCopyFirstValue = () => {
        let monthNumber, monthCurrent;
        const inputList = superRef.current.getElementsByTagName('input');

        for (let input of inputList) {
            monthNumber = input.dataset.month;
            if (monthNumber === '1') {
                monthCurrent = input.value;
            } else {
                input.value = monthCurrent;
            }
        }
    }

    return (
        <div className='mb-3'>
            <div className='row edit-table' ref={superRef}>
                {months.map((month) => {
                    return (
                        <RefInput
                            wrapperClass="col-6"
                            key={month}
                            data-month={month}
                            defaultValue={getValueByMonth(month, 'aim')}
                            preInputElement={`${month}: `}
                        />
                    )
                })}
            </div>

            <button type="button" className="btn btn-primary mt-2"
                onClick={handleOnclick}
            >
                Save months aims
            </button>

            <button type="button" className="btn btn-warning mt-2 ml-3"
                onClick={handleCopyFirstValue}
            >
                Copy value from first field
            </button>
        </div>
    );
}

EditTable.propTypes = {
    monthAims: PropTypes.array.isRequired,
    saveMonthAims: PropTypes.func.isRequired,
    overvalue: PropTypes.number.isRequired,
};
