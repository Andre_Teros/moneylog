import React from "react";
import PropTypes from 'prop-types';
import { useSelector } from 'react-redux';
import { Deposit } from "@andre_teros/deposit";

export const ButtonMoveOverValue = ({ overvalue, saveMonthAims }) => {
    const { chosenYear, allPlansData } = useSelector((state) => ({ ...state.plan }));

    const handleOnClick = () => {
        const nextYear = chosenYear + 1;

        const currentYearPlansData = allPlansData.find((plansData) => plansData.year === chosenYear);
        const nextYearPlansData = allPlansData.find((plansData) => plansData.year === nextYear);

        const nextYearDeposit = new Deposit(
            nextYearPlansData ? nextYearPlansData.month_aims : null,
            nextYearPlansData ? nextYearPlansData.overvalue : 0
        );
        nextYearDeposit.add(currentYearPlansData.overvalue);

        saveMonthAims(currentYearPlansData.month_aims, 0, chosenYear);
        saveMonthAims(nextYearDeposit.plan, nextYearDeposit.overValue, nextYear);
    };


    return (
        <button
            disabled={overvalue <= 0}
            type="button"
            className="btn btn-success ml-2"
            onClick={handleOnClick}
        >
            move overvalue to next year
        </button>
    )
}

ButtonMoveOverValue.propTypes = {
    overvalue: PropTypes.number.isRequired,
    saveMonthAims: PropTypes.func.isRequired,
}
