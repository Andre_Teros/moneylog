import React, { useState, useRef, Fragment } from "react";
import { EditTable } from "./EditTable";
import { RefInput } from "../../common/RefInput";
import { Deposit } from "@andre_teros/deposit";
import PropTypes from "prop-types";
import { useSelector } from 'react-redux';

const ControlBar = ({monthAims, overvalue, saveMonthAims}) => {
    const [isEdit, setIsEdit] = useState(false);

    const inputRef = useRef(null);
    const [error, setError] = useState(null);

    const chosenYear = useSelector((state) => state.plan.chosenYear);

    const handleOnclickAdd = () => {
        const inputValue = Number(inputRef.current.value);

        if (Number.isNaN(inputValue) || inputValue <= 0) {
            setError('value must be positive number');

            return;
        }

        const deposit = new Deposit(monthAims, overvalue);
        deposit.add(inputValue);
        inputRef.current.value = null;
        saveMonthAims(deposit.plan, deposit.overValue, chosenYear);
    }

    const handleOnclickRemove = () => {
        const inputValue = Number(inputRef.current.value);

        if (Number.isNaN(inputValue) || inputValue <= 0) {
            setError('value must be positive number');

            return;
        }

        const deposit = new Deposit(monthAims, overvalue);
        deposit.remove(inputValue);
        inputRef.current.value = null;
        saveMonthAims(deposit.plan, deposit.overValue, chosenYear);
    }

    return (
        <Fragment>
            <div className="row mb-2">
                <RefInput
                    wrapperClass="col-12 col-sm-8 col-md-5"
                    autoComplete="off"
                    placeholder="deposit"
                    inputMode="numeric"
                    ref={inputRef}
                    errorMessage={error}
                />
                <div className="col-12 col-md-7 mt-1 mt-md-0">
                    <button type="button" className="btn btn-primary mr-3" onClick={handleOnclickAdd}>
                        Add deposit
                    </button>

                    <button type="button" className="btn btn-danger mr-3" onClick={handleOnclickRemove}>
                        Remove deposit
                    </button>
                </div>
            </div>

            <button type="button" className="btn btn-success mb-3" onClick={() => setIsEdit(!isEdit)}>
                Edit months aims
            </button>

            {isEdit && <EditTable monthAims={monthAims} saveMonthAims={saveMonthAims} overvalue={overvalue}/>}
        </Fragment>
    );
}

ControlBar.propTypes = {
    monthAims: PropTypes.array,
    overvalue: PropTypes.number,
    saveMonthAims: PropTypes.func,
};

ControlBar.defaultProps = {
    monthAims: null,
};

export { ControlBar };
