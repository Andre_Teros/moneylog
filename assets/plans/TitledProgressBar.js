import React from "react";
import { ProgressBar } from "./ProgressBar";

const TitledProgressBar = ({ current, aim, title }) => {

    return (
        <>
            <p className="progress-bar-title">{title}</p>
            <ProgressBar current={current} aim={aim}/>
        </>
    )
}

export { TitledProgressBar };
