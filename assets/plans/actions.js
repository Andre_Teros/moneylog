export const SET_MONTH_AIMS = 'SET_MONTH_AIMS';
export const SET_CURRENT_YEAR = 'SET_CURRENT_YEAR';

export const setMonthAims = (value) => ({type: SET_MONTH_AIMS, payload: value});
export const setCurrentYear = (year) => ({type: SET_CURRENT_YEAR, payload: year});
