import React, { Component } from "react";
import { connect } from "react-redux";
import { TitledProgressBar } from "./TitledProgressBar";
import { ProgressTable } from "./ProgressTable";
import { fetchPost } from "../common/fetch";
import { successNoty, errorNoty } from "../common/noty";
import { setMonthAims } from "./actions";
import { Deposit } from "@andre_teros/deposit";
import { YearsNav } from "../common/YearsNav";

import "react-loader-spinner/dist/loader/css/react-spinner-loader.css";

class PlansApp extends Component {
    saveMonthAims = (monthAims, overvalue, chosenYear) => {
        const deposit = new Deposit(monthAims, overvalue);
        deposit.recalculate();

        this.props.setMonthAims({
            plan: deposit.plan,
            overvalue: deposit.overValue,
            chosenYear
        });

        fetchPost('/plans/update/' + chosenYear, { monthAims: deposit.plan, overvalue: deposit.overValue })
            .then(() => successNoty('months aims update success'))
            .catch(() => errorNoty('months aims update fails'));
    }

    transformAllPlansData = (allPlansData) => {
        const existedYears = [];
        const indexedAllPlansData = {};
        let totalAim = 0;
        let totalDeposit = 0;

        allPlansData.forEach(yearData => {
            existedYears.push(Number(yearData.year));

            const { year, month_aims: monthAims, overvalue } = yearData
            const deposit = new Deposit(monthAims, overvalue);

            const yearTotalAim = deposit.getTotalAim();
            const yearTotalDeposit = deposit.getTotalDeposit();

            totalAim += yearTotalAim;
            totalDeposit += yearTotalDeposit;

            indexedAllPlansData[year] = {
                yearTotalAim,
                yearTotalDeposit,
                monthAims,
                overvalue,
            }
        });

        return { existedYears, indexedAllPlansData, totalAim, totalDeposit };
    }

    render() {
        const { allPlansData, chosenYear } = this.props;
        const { existedYears, indexedAllPlansData, totalAim, totalDeposit } = this.transformAllPlansData(allPlansData);

        const { yearTotalAim, yearTotalDeposit, monthAims, overvalue } = indexedAllPlansData[chosenYear];

        return (
            <div id="plans-main">
                <YearsNav years={existedYears}/>

                <TitledProgressBar title={`This year (${chosenYear})`} current={yearTotalDeposit} aim={yearTotalAim}/>
                <TitledProgressBar title='Total' current={totalDeposit} aim={totalAim}/>

                <ProgressTable monthAims={monthAims} overvalue={overvalue} saveMonthAims={this.saveMonthAims}/>
            </div>
        );
    }
}

export default connect(
    (state) => ({ ...state.plan }),
    { setMonthAims }
)(PlansApp);
