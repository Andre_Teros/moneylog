import React, {useState} from "react";
import PropTypes from 'prop-types';
import { Input } from '../common/Input';

const AimField = ({aim, saveAim, setAim}) => {
    const [error, setError] = useState(null);

    const handleOnChange = (event) => {
        const inputValue = Number(event.target.value);
        let error = null;

        if (Number.isNaN(inputValue) || inputValue <= 0) {
            error = 'value must be positive number';
        }

        setError(error);
        setAim(inputValue);
    };

    const disabled = error !== null;

    return (
        <div className="row">
            <Input
                wrapperClass="col-8 col-xl-3"
                autoComplete="off"
                placeholder="aim"
                inputMode="numeric"
                value={aim}
                onChange={handleOnChange}
                errorMessage={error}
            />
            <div>
                <button type="button" className="btn btn-primary mr-3" onClick={() => saveAim(aim)} disabled={disabled}>
                    Save aim
                </button>
            </div>
        </div>
    )
};

AimField.propTypes = {
    aim: PropTypes.number,
    saveAim: PropTypes.func,
    setAim: PropTypes.func,
};

AimField.defaultProps = {
    aim: null,
};

export { AimField };
