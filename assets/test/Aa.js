import React, {Component} from "react";

export default class Aa extends Component {
    constructor(props) {
        super(props);

        this.state = {
            color: "#2bce26"
        }
    }

    turnRed = () => {
        this.setState({color: "#ce252a"});
    };

    render() {
        return (
            <div
                style={{
                    width: '200px',
                    height: '200px',
                    backgroundColor: this.state.color,
                }}
            >
            </div>
        );
    }
}
