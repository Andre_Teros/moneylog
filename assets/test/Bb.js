import React, {Component} from "react";
import Aa from './Aa';

export default class Bb extends Component {

    turnRedAa = () => {
        Aa.turnRed();
    };

    render() {
        return (
            <button onClick={this.turnRedAa}>red</button>
        );
    }
}