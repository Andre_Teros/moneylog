import React, {Component, Fragment} from "react";
import Aa from './Aa';
import Bb from './Bb';

export default class Test extends Component {

    handleClick = () => {
        alert('1');
    };

    render() {
        return (
            <Fragment>
                <div onClick={this.handleClick}>kek</div>
                <Aa/>
                <Bb/>
            </Fragment>
        );
    }
}
