import React from "react";
import { render } from 'react-dom';
import Test from './Test';

const dom = document.getElementById('main');

render(<Test/>, dom);
