import React, { Fragment, useState, useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import Collapse from 'react-bootstrap/Collapse';
import { number_format } from "../../common/functions";

const MoneyState = () => {

    const raw = [
        {"sum": "51328.000", "currency": "UAH", "currency_sign": "\u20b4"},
        {"sum": "21000.000", "currency": "USD", "currency_sign": "$"},
        {"sum": "175.000", "currency": "EUR", "currency_sign": "\u20ac"}
    ];

    const moneyState = {
        "shown": {"sum": "51328.000", "currency": "UAH", "currency_sign": "\u20b4"},
        "hidden": [{"sum": "21000.000", "currency": "USD", "currency_sign": "$"}, {
            "sum": "175.000",
            "currency": "EUR",
            "currency_sign": "\u20ac"
        }]
    };

    const [open, setOpen] = useState(false);

    const handleToggle = (event) => {
        event.preventDefault();
        setOpen(!open);
    }

    useEffect(() => {

    }, []);

    return (
        <li className="nav-item no-arrow mx-1 position-relative">
            <a
                className="nav-link"
                href="#"
                onClick={handleToggle}
                aria-controls="money-state-collapse-block"
                aria-expanded={open}
            >
                <i className="fas">{moneyState.shown.currency_sign || moneyState.shown.currency}</i> : {number_format(moneyState.shown.sum, 0, '.', "'")}
            </a>
            <Collapse in={open}>
                <div className="state-block bg-dark" id="money-state-collapse-block">
                    <div>
                        {moneyState.hidden.map((item, key) => (
                            <Fragment key={key}>
                                <i className="fas">{item.currency_sign || item.currency}</i> : {number_format(item.sum, 0, '.', "'")}
                                <br/>
                            </Fragment>
                        ))}
                    </div>
                </div>
            </Collapse>
        </li>
    );
}

export { MoneyState };
