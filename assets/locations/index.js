import React from "react";
import { render } from 'react-dom';
import Locations from './Locations';

const dom = document.getElementById('locations');

render(
    <Locations
        url={dom.dataset.url}
        formAction={dom.dataset.formAction}
    />,
    dom
);
