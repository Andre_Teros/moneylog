import React, {Component, Fragment} from 'react';
import {Button, Dialog, DialogTitle, DialogContent, DialogActions} from '@material-ui/core';
import IconCancel from '@material-ui/icons/Cancel';
import {create} from './actions';

export default class Locations extends Component {
    constructor(props) {
        super(props);

        this.state = {
            dialog: false,
            isLoading: false,
            dataLoaded: true,
        }
    }

    handleOpenClick = () => {
        this.setState({dialog: true, isLoading: !this.state.dataLoaded});
    };

    handleCloseClick = () => {
        this.setState({dialog: false});
    };

    handleSubmit = (event) => {
        event.preventDefault();
        this.setState({isLoading: true});

        const data = new FormData(event.target);
        create(this.props.formAction, data)
            .then((res) => {
                this.setState({isLoading: false});
            })
            .catch((err) => {
                this.setState({isLoading: false});
            })
    };


    render() {
        const {dialog} = this.state;

        return (
            <Fragment>
                <button type="button" className="btn btn-success mr-3" data-url={this.props.url}
                        onClick={this.handleOpenClick}>Location
                </button>
                <button type="button" className="btn btn-success add-form mr-3" data-url={this.props.url}>Add location
                </button>

                <Dialog
                    fullWidth
                    open={dialog}
                    onClose={this.handleCloseClick}
                >
                    <DialogTitle>--- ??? ---</DialogTitle>

                    <form onSubmit={this.handleSubmit}>

                        <DialogContent>
                            <input className="form-control" type="text" name="location[name]" autoComplete="off" placeholder="name"/>
                        </DialogContent>

                        <DialogActions>
                            <Button
                                variant="contained"
                                color="primary"
                                disabled={this.state.isLoading}
                                type="submit"
                            >
                                Save
                            </Button>

                            <Button variant="contained" onClick={this.handleCloseClick}>
                                <IconCancel/>
                                Cancel
                            </Button>
                        </DialogActions>
                    </form>
                </Dialog>

            </Fragment>
        );
    }
}
