export function create(action, data) {
    return fetch(action, {
        method: 'POST',
        body: data,
    })
        .then(res => res)
        .catch(err => err);
}
