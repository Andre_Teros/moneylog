require('webpack');
const path = require('path');
const {CleanWebpackPlugin} = require('clean-webpack-plugin');
const {WebpackManifestPlugin} = require('webpack-manifest-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const CssMinimizerPlugin = require('css-minimizer-webpack-plugin');

module.exports = (env, argv) => {
    const mode = argv.mode || 'production';
    const isProd = mode === 'production';
    const outputPathDir = isProd ? 'build' : 'dev_build';

    const plugins = [
        new CleanWebpackPlugin(),
        new WebpackManifestPlugin({
            basePath: 'build/',
            publicPath: outputPathDir + '/',
        }),
        new MiniCssExtractPlugin({
            filename: isProd ? '[name].[contenthash:6].css' : '[name].css',
        }),
    ];

    if (isProd) {
        plugins.push(new CssMinimizerPlugin());
    }

    return {
        mode,
        entry: {
            plans: './assets/plans/index.js',
        },
        output: {
            path: path.resolve(__dirname, 'public', outputPathDir),
            filename: isProd ? '[name].[chunkhash:6].js' : '[name].js',
            publicPath: '/',
        },
        module: {
            rules: [
                {
                    test: /\.js$/,
                    exclude: /node_modules/,
                    use: {
                        loader: "babel-loader",
                        options: {
                            cacheDirectory: true
                        }
                    }
                },
                {
                    test: /\.css$/,
                    use: [
                        MiniCssExtractPlugin.loader,
                        "css-loader",
                    ],
                },
                {
                    test: /\.(png|jpg|jpeg|gif|ico|svg)$/,
                    use: [
                        {
                            loader: "file-loader",
                            options: {
                                name: 'images/[name].[ext]'
                            }
                        }
                    ]
                },
                {
                    test: /\.(woff|woff2|eot|ttf|otf)$/,
                    use: [
                        {
                            loader: "file-loader",
                            options: {
                                name: 'fonts/[name].[ext]'
                            }
                        }
                    ]
                },
            ]
        },
        plugins,
        devtool: !isProd ? 'source-map' : false,
    };
};
