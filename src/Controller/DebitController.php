<?php
declare(strict_types=1);

namespace App\Controller;

use App\DTO\DebitDTO;
use App\Entity\Type;
use App\Exceptions\NoEntityFound;
use App\Repository\CurrencyRepository;
use App\Repository\DebitRepository;
use App\Repository\SourceRepository;
use App\Repository\TypeRepository;
use App\Service\DebitService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

class DebitController extends AbstractController
{
    public function listAction(
        ?int $month,
        ?int $year,
        DebitRepository $debitRepository
    ): Response {
        $month = $month ?? (int)date('m');
        $year = $year ?? (int)date('Y');

        $metaData = [
            'title' => "debit list [{$month}-{$year}]",
        ];

        $userId = $this->getUser()->getId();
        $data = $debitRepository->findMonthly($userId, $month, $year);

        return $this->render('debit/list.html.twig', [
            'debits' => $data,
            'month' => $month,
            'year' => $year,
            'metaData' => $metaData,
        ]);
    }

    public function newAction(
        TypeRepository $typeRepository,
        CurrencyRepository $currencyRepository,
        SourceRepository $sourceRepository
    ): Response {
        $userId = $this->getUser()->getId();
        $types = $typeRepository->getAllBelongsTo(Type::BELONGS_TO_DEBIT);
        $currencies = $currencyRepository->getAll();
        $sources = $sourceRepository->getAll($userId);

        return $this->render('debit/form.html.twig', [
            'types' => $types,
            'currencies' => $currencies,
            'sources' => $sources,
        ]);
    }

    public function createAction(DebitDTO $dto, DebitService $debitService): JsonResponse
    {
        $userId = $this->getUser()->getId();

        $em = $this->getDoctrine()->getManager();

        $debitService->create($dto, $userId);

        $em->flush();

        return $this->json(['status' => 'ok']);
    }

    public function deleteAction($id, DebitService $debitService): JsonResponse
    {
        try {
            $debitService->delete((int)$id);
        } catch (NoEntityFound $exception) {
            return $this->json(['status' => 'fail']);
        }

        $em = $this->getDoctrine()->getManager();

        $em->flush();

        return $this->json(['status' => 'ok']);
    }
}
