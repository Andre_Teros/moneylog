<?php
declare(strict_types=1);

namespace App\Controller;

use App\Service\DashboardService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;

class DashboardController extends AbstractController
{
    public function indexAction(DashboardService $dashboardService): Response
    {
        $metaData = [
            'title' => 'dashboard',
        ];

        $userId = $this->getUser()->getId();
        $capitalLocationData = $dashboardService->getCapitalLocationData($userId);
        $capitalData = $dashboardService->getCapitalData($userId);
        $groupedCapitalData = $dashboardService->getGroupedCapitalData($userId);
        $stateData = $dashboardService->getStateData($userId);
        $debitData = $dashboardService->getDebitData($userId);
        $creditData = $dashboardService->getCreditData($userId);

        return $this->render('default/dashboard.html.twig', [
            'metaData' => $metaData,
            'capitalLocationData' => $capitalLocationData,
            'capitalData' => $capitalData,
            'groupedCapitalData' => $groupedCapitalData,
            'stateData' => $stateData,
            'debitData' => $debitData,
            'creditData' => $creditData,
        ]);
    }
}
