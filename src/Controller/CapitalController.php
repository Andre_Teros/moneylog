<?php
declare(strict_types=1);

namespace App\Controller;

use App\DTO\CapitalLocationDTO;
use App\Entity\CapitalLocation;
use App\Repository\CapitalLocationRepository;
use App\Repository\CurrencyRepository;
use App\Repository\CapitalRepository;
use App\Service\CapitalService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class CapitalController extends AbstractController
{
    public function formAction(
        CurrencyRepository $currencyRepository,
        CapitalLocationRepository $capitalLocationRepository,
        CapitalRepository $capitalRepository
    ): Response {
        $metaData = [
            'title' => 'capital create',
        ];

        $userId = $this->getUser()->getId();
        $lastState = $capitalRepository->findLastState($userId);

        $currencies = $currencyRepository->getAll();
        $capitalLocations = $capitalLocationRepository->getAll($userId);

        $stateData = $this->getStateData($capitalLocations, $currencies, $lastState);

        return $this->render('capital/form.html.twig', [
            'capital_locations' => $capitalLocations,
            'stateData' => $stateData,
            'metaData' => $metaData,
        ]);
    }

    public function createAction(
        Request $request,
        CapitalService $capitalService,
        CapitalRepository $capitalRepository
    ): JsonResponse {
        if ($this->isCsrfTokenValid('capital_form', $request->request->get('_csrf_token'))) {
            $requestData = $request->request->get('capital');
            $userId = $this->getUser()->getId();
            $em = $this->getDoctrine()->getManager();
            $lastStateDate = $capitalRepository->findLastStateDate($userId);

            if ($lastStateDate === null || $lastStateDate != new \DateTime('today')) {
                $capitalService->create($requestData, $userId);
            } else {
                $capitalService->update($requestData, $userId);
            }

            $em->flush();
        }

        return $this->json(['status' => 'ok']);
    }

    public function newLocationAction(): Response
    {
        return $this->render('capital/location_form.html.twig');
    }

    public function createLocationAction(CapitalLocationDTO $dto): JsonResponse
    {
        $em = $this->getDoctrine()->getManager();
        $userId = $this->getUser()->getId();
        $capitalLocation = new CapitalLocation($dto->name, $userId);
        $em->persist($capitalLocation);
        $em->flush();

        return $this->json(['status' => 'ok']);
    }

    private function getStateData(array $capitalLocations, array $currencies, array $currentState): array
    {
        $state = [];

        $transformedCurrentState = [];
        foreach ($currentState as $stateRow) {
            $transformedCurrentState[$stateRow['currency']][$stateRow['location']] = $stateRow['sum'];
        }

        foreach ($currencies as $currency) {
            $currencyName = $currency['name'];
            foreach ($capitalLocations as $location) {
                $locationName = $location['name'];
                $state[$currencyName][$locationName] = [
                    'sum' => $transformedCurrentState[$currencyName][$locationName] ?? null,
                    'key' => "[{$currency['id']}][{$location['id']}]",
                ];
            }
        }

        return $state;
    }
}
