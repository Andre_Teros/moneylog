<?php
declare(strict_types=1);

namespace App\Controller;

use App\DTO\DebtDTO;
use App\Entity\Debt;
use App\Repository\CurrencyRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

class DebtController extends AbstractController
{
    private $eventDispatcher;

    public function __construct(EventDispatcherInterface $eventDispatcher)
    {
        $this->eventDispatcher = $eventDispatcher;
    }

    public function list(): Response
    {
        $metaData = [
            'title' => 'debt list',
        ];
        $em = $this->getDoctrine()->getManager();
        $userId = $this->getUser()->getId();
        $data = $em->getRepository(Debt::class)->findBy(['user_id' => $userId]);

        return $this->render('debt/list.html.twig', [
            'debts' => $data,
            'metaData' => $metaData,
        ]);
    }

    public function new(CurrencyRepository $currencyRepository): Response
    {
        $currencies = $currencyRepository->getAll();

        return $this->render('debt/form.html.twig', [
            'currencies' => $currencies,
        ]);
    }

    public function create(DebtDTO $debtDTO, CurrencyRepository $currencyRepository): JsonResponse
    {
        $em = $this->getDoctrine()->getManager();
        $userId = $this->getUser()->getId();
        $currency = $currencyRepository->get((int) $debtDTO->currency);
        $date = $debtDTO->date ? \DateTime::createFromFormat('Y-m-d', $debtDTO->date) : null;

        $debt = new Debt(
            (float)$debtDTO->sum,
            $currency,
            $userId,
            $debtDTO->debtor,
            $date
        );

        foreach ($debt->popEvents() as $event) {
            $this->eventDispatcher->dispatch($event);
        }
        $em->persist($debt);
        $em->flush();

        return $this->json([
            'status' => 'ok',
        ]);
    }

    public function repay($id, $mark): JsonResponse
    {
        $em = $this->getDoctrine()->getManager();
        /** @var Debt $debt */
        $debt = $em->find(Debt::class, $id);

        if ($debt === null) {
            return $this->json(['status' => 'fail']);
        }

        $debt->repay($mark);
        foreach ($debt->popEvents() as $event) {
            $this->eventDispatcher->dispatch($event);
        }
        $em->flush();

        return $this->json(['status' => 'ok']);
    }
}
