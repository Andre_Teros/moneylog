<?php
declare(strict_types=1);

namespace App\Controller\Component;

use App\Service\YearListService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;

class YearsNavigator extends AbstractController
{
    public function indexAction(string $basePath, int $chosenMonth, int $chosenYear, YearListService $yearListService): Response
    {
        $userId = $this->getUser()->getId();

        return $this->render('template/sbadmin_inc/years_nav.html.twig', [
            'basePath' => $basePath,
            'years' => $yearListService->get($userId),
            'chosenMonth' => $chosenMonth,
            'chosenYear' => $chosenYear,
        ]);
    }
}
