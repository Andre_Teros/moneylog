<?php
declare(strict_types=1);

namespace App\Controller\Component;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;

class Menu extends AbstractController
{
    private static $menu = [
        [
            'type' => 'single',
            'route' => 'dashboard',
            'name' => 'dashboard',
            'icon' => 'fa-tachometer-alt'
        ],
        [
            'type' => 'single',
            'route' => 'credit_list',
            'name' => 'credit list',
            'icon' => 'fa-table'
        ],
        [
            'type' => 'single',
            'route' => 'debit_list',
            'name' => 'debit list',
            'icon' => 'fa-table'
        ],
        [
            'type' => 'single',
            'route' => 'debt_list',
            'name' => 'debt list',
            'icon' => 'fa-list'
        ],
        [
            'type' => 'single',
            'route' => 'plans',
            'name' => 'plans',
            'icon' => 'fa-chart-line'
        ],
//        [
//            'type' => 'multi',
//            'name' => 'Pages',
//            'items' => [
//                [
//                    'type' => 'single',
//                    'route' => 'credit_list',
//                    'name' => 'credit list',
//                ],
//            ]
//        ],
    ];

    public function indexAction($currentRoute): Response
    {
        return $this->render('template/sbadmin_inc/sidebar.html.twig', [
            'menuItems' => $this->getMenuItems(null, $currentRoute),
        ]);
    }

    private function getMenuItems($items = null, $currentRoute = null)
    {
        $items = $items ?? self::$menu;

        $router = $this->get('router');

        foreach ($items as &$item) {
            if ($item['type'] === 'single') {
                $item['url'] = !empty($item['route']) ? $router->generate($item['route']) : '#';
                $item['active'] = $item['route'] === $currentRoute;
            } else {
                $item['items'] = $this->getMenuItems($item['items'], $currentRoute);
                $item['active'] = false;
            }
        }

        return $items;
    }
}
