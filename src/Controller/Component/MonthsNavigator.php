<?php
declare(strict_types=1);

namespace App\Controller\Component;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;

class MonthsNavigator extends AbstractController
{
    public function indexAction(string $basePath, int $chosenMonth, int $chosenYear): Response
    {
        return $this->render('template/sbadmin_inc/months_nav.html.twig', [
            'basePath' => $basePath,
            'chosenMonth' => $chosenMonth,
            'chosenYear' => $chosenYear,
        ]);
    }
}
