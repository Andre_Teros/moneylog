<?php
declare(strict_types=1);

namespace App\Controller\Component;

use App\Entity\Currency;
use App\Repository\StateRepository;
use App\Templating\State\StateToNavBar;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;

class NavBar extends AbstractController
{
    public function indexAction(StateRepository $stateRepository, StateToNavBar $stateToNavBar): Response
    {
        $userId = $this->getUser()->getId();
        $stateData = $stateRepository->findByUser($userId);

        return $this->render('template/sbadmin_inc/navbar.html.twig', [
            'stateData' => $stateToNavBar->transform($stateData, Currency::DEFAULT),
        ]);
    }
}
