<?php
declare(strict_types=1);

namespace App\Controller;

use App\Service\PlanService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class PlansController extends AbstractController
{
    private $service;

    public function __construct(PlanService $service)
    {
        $this->service = $service;
    }

    public function index(): Response
    {
        $userId = $this->getUser()->getId();
        $res = $this->service->getPlan($userId);

        return $this->render('plans.html.twig', [
            'initData' => json_encode($res),
            'metaData' => ['title' => 'plans'],
        ]);
    }

    public function update(Request $request, string $year): JsonResponse
    {
        $userId = $this->getUser()->getId();
        $requestContent = json_decode($request->getContent(), true);

        $result = $this->service->savePlan($requestContent, $userId, (int) $year);
        $status = $result ? 200 : 500;

        return new JsonResponse(['ok' => 'ok'], $status);
    }

    public function getByYear(string $year): JsonResponse
    {
        $userId = $this->getUser()->getId();
        $res = $this->service->getPlan($userId, (int) $year);

        return new JsonResponse($res);
    }
}
