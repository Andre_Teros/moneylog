<?php
declare(strict_types=1);

namespace App\Controller;

use App\DTO\CreditDTO;
use App\Entity\Type;
use App\Exceptions\NoEntityFound;
use App\Service\CreditService;
use App\Repository\CreditRepository;
use App\Repository\CurrencyRepository;
use App\Repository\TypeRepository;
use App\Templating\Credit\JexcelDataTransformer;
use App\Templating\Credit\TableDataTransformer;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

class CreditController extends AbstractController
{
    public function listAction(
        ?int $month,
        ?int $year,
        CreditRepository $creditRepository,
        JexcelDataTransformer $dataTransformer
    ): Response {
        $month = $month ?? (int)date('m');
        $year = $year ?? (int)date('Y');

        $metaData = [
            'title' => "credit list [{$month}-{$year}]",
        ];

        $userId = $this->getUser()->getId();
        $rawData = $creditRepository->findMonthlyGrouped($userId, $month, $year);
        $groupedData = $dataTransformer->transformGrouped($rawData);

        $rawDetailData = $creditRepository->findMonthly($userId, $month, $year);
        $groupedDetailData = $dataTransformer->transformDetail($rawDetailData);

        return $this->render('credit/list.html.twig', [
            'credits' => json_encode($groupedData),
            'creditsDetail' => json_encode($groupedDetailData),
            'month' => $month,
            'year' => $year,
            'metaData' => $metaData,
        ]);
    }

    public function newAction(
        TypeRepository $typeRepository,
        CurrencyRepository $currencyRepository,
        CreditRepository $creditRepository
    ): Response {
        $userId = $this->getUser()->getId();
        $tags = $creditRepository->findTagsByUser($userId);
        array_unshift($tags, '');
        $types = $typeRepository->getAllBelongsTo(Type::BELONGS_TO_CREDIT);
        $currencies = $currencyRepository->getAll();

        return $this->render('credit/form.html.twig', [
            'types' => $types,
            'currencies' => $currencies,
            'tags' => $tags,
        ]);
    }

    public function createAction(CreditDTO $dto, CreditService $creditService): JsonResponse
    {
        $userId = $this->getUser()->getId();

        $em = $this->getDoctrine()->getManager();

        $creditService->create($dto, $userId);

        $em->flush();

        return $this->json(['status' => 'ok']);
    }

    public function deleteAction($id, CreditService $creditService): JsonResponse
    {
        try {
            $creditService->delete((int)$id);
        } catch (NoEntityFound $exception) {
            return $this->json(['status' => 'fail']);
        }

        $em = $this->getDoctrine()->getManager();

        $em->flush();

        return $this->json(['status' => 'ok']);
    }

    public function yearListAction(
        ?int $year,
        CreditRepository $creditRepository,
        CurrencyRepository $currencyRepository,
        TableDataTransformer $transformer
    ) {
        $userId = $this->getUser()->getId();

        $rawCreditData = $creditRepository->findMonthlyTotalForYear($userId, $year);
        $currencyData = $currencyRepository->getAll(['name', 'order']);
        $transformedData = $transformer->transformSeveralMonth($rawCreditData, $currencyData);

        return $this->render('credit/year_list.html.twig', [
            'creditData' => $transformedData,
        ]);
    }
}
