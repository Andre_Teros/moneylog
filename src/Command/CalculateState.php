<?php
declare(strict_types=1);

namespace App\Command;

use App\Repository\UserRepository;
use App\Service\StateService;
use Doctrine\DBAL\Driver\Connection;
use Doctrine\ORM\EntityManagerInterface;
use Latitude\QueryBuilder\QueryFactory;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Exception\InvalidArgumentException;
use Symfony\Component\Console\Command\Command;
use function Latitude\QueryBuilder\alias;
use function Latitude\QueryBuilder\field;
use function Latitude\QueryBuilder\func;

class CalculateState extends Command
{
    protected static $defaultName = 'app:state:calculate';

    private $qb;
    private $connection;
    private $userRepository;
    private $stateService;
    private $em;

    private $userId;

    public function __construct(
        QueryFactory $qb,
        Connection $connection,
        UserRepository $userRepository,
        StateService $stateService,
        EntityManagerInterface $em
    ) {
        parent::__construct();
        $this->qb = $qb;
        $this->connection = $connection;
        $this->userRepository = $userRepository;
        $this->stateService = $stateService;
        $this->em = $em;
    }

    protected function configure(): void
    {
        $this->addOption('user', 'u', InputOption::VALUE_REQUIRED);
    }

    protected function interact(InputInterface $input, OutputInterface $output): void
    {
        $this->userId = (int)$input->getOption('user');
        $this->validateOption();
    }

    protected function execute(InputInterface $input, OutputInterface $output): void
    {
        $credits = $this->getCredits();
        $debits = $this->getDebits();
        $this->clearState();

        foreach ($credits as $credit) {
            $this->stateService->decrease($this->userId, (int)$credit['currency_id'], (float)$credit['sum']);
        }

        $this->em->flush();
        $this->em->clear();

        foreach ($debits as $debit) {
            $this->stateService->increase($this->userId, (int)$debit['currency_id'], (float)$debit['sum']);
        }

        $this->em->flush();
    }

    /**
     * @throws InvalidArgumentException
     */
    private function validateOption(): void
    {
        $userEntity = $this->userRepository->find($this->userId);
        if ($userEntity === null) {
            throw new InvalidArgumentException("user does not exist, id - [{$this->userId}]");
        }
    }

    private function getCredits(): array
    {
        $query = $this->qb
            ->select(
                'currency_id',
                alias(func('SUM', 'sum'), 'sum')
            )
            ->from('credits')
            ->where(field('user_id')->eq($this->userId))
            ->groupBy('currency_id')
            ->compile();

        $stmt = $this->connection->prepare($query->sql());
        $stmt->execute($query->params());

        return $stmt->fetchAll();
    }

    private function getDebits(): array
    {
        $query = $this->qb
            ->select(
                'currency_id',
                alias(func('SUM', 'sum'), 'sum')
            )
            ->from('debits')
            ->where(field('user_id')->eq($this->userId))
            ->groupBy('currency_id')
            ->compile();

        $stmt = $this->connection->prepare($query->sql());
        $stmt->execute($query->params());

        return $stmt->fetchAll();
    }

    private function clearState()
    {
        $query = $this->qb
            ->delete('states')
            ->where(field('user_id')->eq($this->userId))
            ->compile();

        $stmt = $this->connection->prepare($query->sql());
        $stmt->execute($query->params());
    }
}
