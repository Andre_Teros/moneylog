<?php
declare(strict_types=1);

namespace App\Command;

use App\Entity\Source;
use App\Repository\CurrencyRepository;
use App\Repository\SourceRepository;
use App\Repository\TypeRepository;
use App\Repository\UserRepository;
use Doctrine\DBAL\Driver\Connection;
use Doctrine\ORM\EntityManagerInterface;
use Latitude\QueryBuilder\QueryFactory;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Exception\InvalidArgumentException;

class UploadDebitCommand extends ContainerAwareCommand
{
    private const UNKNOWN_SOURCE = 'unknown';
    private const EXCHANGE_TAG = 'exchange';

    private $qb;
    private $connection;
    private $typeRepository;
    private $currencyRepository;
    private $userRepository;
    private $sourceRepository;
    private $em;

    private $file;
    private $userId;
    private $currencyId;
    private $typeId;
    private $creditTypeId;
    private $year;

    protected static $defaultName = 'upload:debit';

    public function __construct(
        QueryFactory $qb,
        Connection $connection,
        TypeRepository $typeRepository,
        CurrencyRepository $currencyRepository,
        UserRepository $userRepository,
        SourceRepository $sourceRepository,
        EntityManagerInterface $em
    ) {
        parent::__construct();
        $this->qb = $qb;
        $this->connection = $connection;
        $this->typeRepository = $typeRepository;
        $this->currencyRepository = $currencyRepository;
        $this->userRepository = $userRepository;
        $this->sourceRepository = $sourceRepository;
        $this->em = $em;
    }

    protected function configure(): void
    {
        $this->addOption('file', 'f', InputOption::VALUE_REQUIRED)
            ->addOption('year', 'y', InputOption::VALUE_REQUIRED)
            ->addOption('user', 'u', InputOption::VALUE_REQUIRED);
    }

    protected function interact(InputInterface $input, OutputInterface $output): void
    {
        $rootDir = $this->getContainer()->getParameter('kernel.project_dir');
        $this->file = "{$rootDir}/" . $input->getOption('file');
        $this->year = $input->getOption('year');
        $this->userId = (int)$input->getOption('user');

        $this->validateOption();

        $this->initData();
    }

    protected function execute(InputInterface $input, OutputInterface $output): void
    {
        $handle = fopen($this->file, 'rb');

        $debitInsert = $this->qb->insert('debits')
            ->columns(...['currency_id', 'type_id', 'source_id','sum', 'user_id', 'date']);

        $creditInsert = $this->qb->insert('credits')
            ->columns(...['currency_id', 'type_id', 'sum', 'user_id', 'tag', 'date']);

        $date = "{$this->year}-01-01";

        while ($data = fgetcsv($handle,0,';','"',"\n")) {
            if (!empty($data[0])) {
                if (preg_match('/^-?\d*\,?\.?\d*$/', $data[0]) === 0) {
                    continue;
                }

                $sum = str_replace(',', '.', $data[0]);

                if ($sum > 0) {
                    if (empty($data[1])) {
                        $sourceName = self::UNKNOWN_SOURCE;
                    } else {
                        $parsedData = $this->parseSouceAndDate($data[1]);
                        $sourceName = $parsedData['source'];
                        if ($parsedData['date'] !== null) {
                            $date = "{$this->year}-{$parsedData['date']}";
                        }
                    }

                    $sourceId = $this->getSourceId($sourceName);
                    $debitInsert->values(
                        $this->currencyId,
                        $this->typeId,
                        $sourceId,
                        $sum,
                        $this->userId,
                        $date
                    );
                } else {
                    $creditInsert->values(
                        $this->currencyId,
                        $this->creditTypeId,
                        -(float)$sum,
                        $this->userId,
                        self::EXCHANGE_TAG,
                        $date
                    );
                }
            }
        }

        $stmt = $this->connection->prepare($debitInsert->compile()->sql());
        $stmt->execute($debitInsert->compile()->params());

        $stmt = $this->connection->prepare($creditInsert->compile()->sql());
        $stmt->execute($creditInsert->compile()->params());
    }

    private function initData(): void
    {
        $typeEntity = $this->typeRepository->findOneBy(['name' => 'profit']);
        $this->typeId = $typeEntity->getId();

        $typeEntity = $this->typeRepository->findOneBy(['name' => 'irregular']);
        $this->creditTypeId = $typeEntity->getId();

        $currencyEntity = $this->currencyRepository->findOneBy(['name' => 'UAH']);
        $this->currencyId = $currencyEntity->getId();
    }

    /**
     * @throws InvalidArgumentException
     */
    private function validateOption(): void
    {
        if (empty($this->file) || empty($this->year) || empty($this->userId)) {
            throw new InvalidArgumentException('all options are necessary');
        }

        if(preg_match('/^\d{4}$/', $this->year) === 0) {
            throw new InvalidArgumentException('format of year in not correct');
        }

        if(!file_exists($this->file)) {
            throw new InvalidArgumentException('file does not exist');
        }

        $userEntity = $this->userRepository->find($this->userId);
        if ($userEntity === null) {
            throw new InvalidArgumentException("user does not exist, id - [{$this->userId}]");
        }
    }

    private function getSourceId(string $sourceName): int
    {
        $sourceEntity = $this->sourceRepository->findOneBy([
            'name' => $sourceName,
            'user_id' => $this->userId,
        ]);

        if ($sourceEntity === null) {
            $sourceEntity = new Source($sourceName, $this->userId);
            $this->em->persist($sourceEntity);
            $this->em->flush();
        }

        return $sourceEntity->getId();
    }

    private function parseSouceAndDate($rawData): array
    {
        if (!preg_match('/^(?<s>[a-zA-Zа-яА-Я-\s]*)(\((?<d>\d{2})\.(?<m>\d{2})\))?$/u', $rawData, $matches)) {
            return [
                'source' => self::UNKNOWN_SOURCE,
                'date' => null
            ];
        }

        if (!empty($matches['d']) && !empty($matches['m'])) {
            $date = "{$matches['m']}-{$matches['d']}";
        } else {
            $date = null;
        }

        return [
            'source' => $matches['s'],
            'date' => $date,
        ];
    }
}
