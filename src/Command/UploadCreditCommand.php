<?php
declare(strict_types=1);

namespace App\Command;

use App\Entity\Type;
use App\Repository\CurrencyRepository;
use App\Repository\TypeRepository;
use App\Repository\UserRepository;
use Doctrine\DBAL\Driver\Connection;
use Latitude\QueryBuilder\QueryFactory;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Exception\InvalidArgumentException;

class UploadCreditCommand extends ContainerAwareCommand
{
    private $qb;
    private $connection;
    private $typeRepository;
    private $currencyRepository;
    private $userRepository;

    private $file;
    private $userId;
    private $currencyId;
    private $types;
    private $year;

    protected static $defaultName = 'upload:credit';

    public function __construct(QueryFactory $qb, Connection $connection, TypeRepository $typeRepository, CurrencyRepository $currencyRepository, UserRepository $userRepository)
    {
        parent::__construct();
        $this->qb = $qb;
        $this->connection = $connection;
        $this->typeRepository = $typeRepository;
        $this->currencyRepository = $currencyRepository;
        $this->userRepository = $userRepository;
    }

    protected function configure(): void
    {
        $this->addOption('file', 'f', InputOption::VALUE_REQUIRED)
            ->addOption('year', 'y', InputOption::VALUE_REQUIRED)
            ->addOption('user', 'u', InputOption::VALUE_REQUIRED);
    }

    protected function interact(InputInterface $input, OutputInterface $output): void
    {
        $rootDir = $this->getContainer()->getParameter('kernel.project_dir');
        $this->file = "{$rootDir}/" . $input->getOption('file');
        $this->year = $input->getOption('year');
        $this->userId = (int)$input->getOption('user');

        $this->validateOption();

        $this->initData();
    }

    protected function execute(InputInterface $input, OutputInterface $output): void
    {
        $handle = fopen($this->file, 'rb');

        $st = $this->qb->insert('credits')
            ->columns(...['currency_id', 'type_id', 'sum', 'user_id', 'tag', 'date']);

        $date = "{$this->year}-01-01";

        while ($data = fgetcsv($handle,0,';','"',"\n")) {
            if (preg_match('/\"(?<m>\d{2})\"/', $data[0], $matches)) {
                $date = "{$this->year}-{$matches['m']}-01";
                continue;
            }

            if (!empty($data[0])) {
                $tag = !empty($data[1]) ? $data[1] : 'unknown';

                $st->values(
                    $this->currencyId,
                    $this->types['regular'],
                    $data[0],
                    $this->userId,
                    $tag,
                    $date
                );
            }

            if (!empty($data[2])) {
                $tag = !empty($data[3]) ? $data[3] : 'unknown';

                $st->values(
                    $this->currencyId,
                    $this->types['irregular'],
                    $data[2],
                    $this->userId,
                    $tag,
                    $date
                );
            }
        }

        $stmt = $this->connection->prepare($st->compile()->sql());
        $stmt->execute($st->compile()->params());
    }

    private function initData(): void
    {
        $types = $this->typeRepository->getAllBelongsTo(Type::BELONGS_TO_CREDIT);
        $this->types = array_combine(
            array_column($types, 'name'),
            array_column($types, 'id')
        );

        $currencyEntity = $this->currencyRepository->findOneBy(['name' => 'UAH']);
        $this->currencyId = $currencyEntity->getId();
    }

    /**
     * @throws InvalidArgumentException
     */
    private function validateOption(): void
    {
        if (empty($this->file) || empty($this->year) || empty($this->userId)) {
            throw new InvalidArgumentException('all options are necessary');
        }

        if(preg_match('/^\d{4}$/', $this->year) === 0) {
            throw new InvalidArgumentException('format of year in not correct');
        }

        if(!file_exists($this->file)) {
            throw new InvalidArgumentException('file does not exist');
        }

        $userEntity = $this->userRepository->find($this->userId);
        if ($userEntity === null) {
            throw new InvalidArgumentException("user does not exist, id - [{$this->userId}]");
        }
    }
}
