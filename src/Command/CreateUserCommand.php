<?php
declare(strict_types=1);

namespace App\Command;

use App\Entity\CapitalLocation;
use App\Entity\Source;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Security\Core\Encoder\BCryptPasswordEncoder;

class CreateUserCommand extends Command
{
    private $em;
    private $encoder;

    private $email;
    private $pass;

    protected static $defaultName = 'app:add-user';

    public function __construct(EntityManagerInterface $em, BCryptPasswordEncoder $encoder)
    {
        parent::__construct();
        $this->em = $em;
        $this->encoder = $encoder;
    }

    protected function configure()
    {
        $this->addArgument('email', InputArgument::REQUIRED)
            ->addArgument('pass', InputArgument::REQUIRED);
    }

    protected function interact(InputInterface $input, OutputInterface $output)
    {
        // TODO: Validation
        $this->email = $input->getArgument('email');
        $this->pass = $input->getArgument('pass');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $pass = $this->encoder->encodePassword($this->pass, '');
        $user = new User($this->email, $pass, ['ROLE_USER']);

        $this->em->persist($user);
        $this->em->flush();
        $this->em->clear();

        $userId = $user->getId();

        $location = new CapitalLocation('home', $userId);
        $this->em->persist($location);

        $source = new Source('work', $userId);
        $this->em->persist($source);

        $this->em->flush();

        $output->writeln("User has created, id - [{$userId}]");
    }
}
