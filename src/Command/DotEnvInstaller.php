<?php
declare(strict_types=1);

namespace App\Command;

class DotEnvInstaller
{
    private const DEFAULT_ENV = '.env';
    private const DEFAULT_DIST = '.dist';

    public static function buildEnv(): void
    {
        if (file_exists(self::DEFAULT_ENV)) {
            return;
        }
        copy(self::DEFAULT_ENV . self::DEFAULT_DIST, self::DEFAULT_ENV);
    }
}
