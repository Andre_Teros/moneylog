<?php
declare(strict_types=1);

namespace App\Command;

use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Exception\LogicException;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class DeleteUserCommand extends Command
{
    private $em;
    private $userRepository;

    private $userId;

    protected static $defaultName = 'app:del-user';

    public function __construct(EntityManagerInterface $em, UserRepository $userRepository)
    {
        parent::__construct();
        $this->em = $em;
        $this->userRepository = $userRepository;
    }

    protected function configure()
    {
        $this->addOption('id', null, InputOption::VALUE_REQUIRED);
    }

    protected function interact(InputInterface $input, OutputInterface $output)
    {
        $this->userId = (int)$input->getOption('id');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $user = $this->userRepository->find($this->userId);

        if ($user === null) {
            throw new LogicException("user not found id - [{$this->userId}]");
        }

        $this->em->remove($user);
        $this->em->flush();

        $output->writeln("User has deleted, id - [{$this->userId}]");
    }
}
