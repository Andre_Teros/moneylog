<?php
declare(strict_types=1);

namespace App\Exceptions;

class NoDataFound extends DBException
{
    public const Message = 'Exception: No data Found';
}
