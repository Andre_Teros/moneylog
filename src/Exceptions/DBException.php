<?php
declare(strict_types=1);

namespace App\Exceptions;

class DBException extends \Exception
{
    public const Message = 'Database exception';

    public function __construct(string $method, array $arguments, string $query, array $params = [])
    {
        $message = json_encode([
            'message' => static::Message,
            'method' => $method,
            'arguments' => $arguments,
            'query' => $query,
            'params' => $params,
        ]);
        parent::__construct($message);
    }
}
