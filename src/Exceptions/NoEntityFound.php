<?php
declare(strict_types=1);

namespace App\Exceptions;

class NoEntityFound extends \Exception
{
    private $entityId;
    private $entityClass;

    public function __construct($entityId, $entityClass, string $message = "")
    {
        parent::__construct($message);
        $this->entityId = $entityId;
        $this->entityClass = $entityClass;
    }

    public function getEntityId()
    {
        return $this->entityId;
    }

    public function getEntityClass()
    {
        return $this->entityClass;
    }
}
