<?php
declare(strict_types=1);

namespace App\Exceptions;

use Symfony\Component\Validator\ConstraintViolationListInterface;

class WrongValidation extends \Exception
{
    private $formId;
    private $errorList;

    public function __construct(string $formId, ConstraintViolationListInterface $errorList)
    {
        $this->formId = $formId;
        $this->errorList = $errorList;
        parent::__construct('wrong validation');
    }

    public function getErrorList(): ConstraintViolationListInterface
    {
        return $this->errorList;
    }

    public function getFullPropertyPath(string $propertyPath): string
    {
        return "{$this->formId}[{$propertyPath}]";
    }
}
