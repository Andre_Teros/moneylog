<?php
declare(strict_types=1);

namespace App\Templating\State;

class StateToNavBar
{
    public function transform(array $rawData, string $defaultCurrency): array
    {
        $resultData = [];

        foreach ($rawData as $state) {
            if ($state['currency'] === $defaultCurrency) {
                $resultData['shown'] = $state;
            } else {
                $resultData['hidden'][] = $state;
            }
        }

        return $resultData;
    }
}
