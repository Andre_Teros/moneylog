<?php
declare(strict_types=1);

namespace App\Templating\Credit;

use App\DTO\CreditIndexes;
use App\DTO\CreditUIGrouped;

class JexcelDataTransformer
{
    private $nestedHeaders = [];
    private $colHeaders = [];
    private $colWidths;

    private $usedTypes = [];

    public function transformGrouped(array $rawData): ?CreditUIGrouped
    {
        if (empty($rawData)) {
            return null;
        }

        $res = $this->groupCreditsData($rawData);
        $this->initColumnsAndHeaders();

        return new CreditUIGrouped(
            $res['groupedData'],
            $res['metaData'],
            [$this->nestedHeaders],
            $this->colHeaders,
            $this->colWidths
        );
    }

    public function transformDetail(array $rawData): array
    {
        return $this->groupCreditsDetailData($rawData);
    }

    private function groupCreditsData(array $rawData): array
    {
        $groupedData = [];
        $metaData = [];

        $currentType = '';
        $i = -1;

        foreach ($rawData as $creditData) {
            if ($currentType !== $creditData['type']) {
                $i++;
                $indexes = CreditIndexes::fromIndex($i);
                $this->usedTypes[] = $creditData['type'];
            }

            $typeCounter = $indexes->getNextCounter();

            $groupedData[$i]["_{$typeCounter}"] = [
                $indexes->sum => $creditData['sum'] . ' ' . ($creditData['currency_sign'] ?: $creditData['currency_name']),
                $indexes->tag => $creditData['tag'],
            ];

            $detailKey = md5($creditData['type'] . $creditData['currency_name'] . $creditData['tag']);
            $metaData["_{$indexes->sum}{$typeCounter}"] = [
                'detailKey' => $detailKey,
                'title' => $creditData['tag'],
            ];
            $metaData["_{$indexes->tag}{$typeCounter}"] =
                $metaData["_{$indexes->sum}{$typeCounter}"];


            $currentType = $creditData['type'];
        }

        return [
            'groupedData' => array_values(array_merge_recursive(...$groupedData)),
            'metaData' => $metaData,
        ];
    }

    private function initColumnsAndHeaders(): void
    {
        foreach ($this->usedTypes as $type) {
            $this->nestedHeaders[] = [
                'title' => $type,
                'colspan' => '2',
            ];

            $this->colHeaders[] = 'sum';
            $this->colHeaders[] = 'tag';
        }

        $this->colWidths = array_fill(0, count($this->usedTypes) * 2, 100);
    }

    private function groupCreditsDetailData(array $rawData): array
    {
        $groupedData = [];
        foreach ($rawData as $creditData) {
            $key = md5($creditData['type'] . $creditData['currency_name'] . $creditData['tag']);
            $groupedData[$key]['detailData'][] = [
                $creditData['sum'] . ' ' . ($creditData['currency_sign'] ?: $creditData['currency_name']),
                $creditData['date'],
                $creditData['description'],
            ];

            $groupedData[$key]['metaData'][] = [
                'id' => $creditData['id'],
            ];
        }

        return $groupedData;
    }
}
