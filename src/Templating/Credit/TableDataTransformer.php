<?php
declare(strict_types=1);

namespace App\Templating\Credit;

use App\Repository\CreditRepository;

class TableDataTransformer implements IDataTransformer
{
    public const LAST_ELEMENT = 999;

    private static $currencySortIndex;
    private static $typeSortIndex;
    private static $typeCount;

    private static $totalColumn = 'total';
    private static $totalColumns;

    public function __construct()
    {
        $this->init();
    }

    public function transform(array $rawCreditData, array $currencyData): array
    {
        $groupedData = [
            'head' => $this->getTableHead(),
            'body' => [],
        ];

        if (empty($rawCreditData)) {
            return $groupedData;
        }

        $this->prepareCurrencySortIndex($currencyData);

        $groupedData['body'] = $this->getTableBody($rawCreditData);

        return $groupedData;
    }

    public function transformSeveralMonth(array $rawCreditData, array $currencyData): array
    {
        $groupedData = [
            'head' => $this->getTableHead(),
            'body' => [],
        ];

        if (empty($rawCreditData)) {
            return $groupedData;
        }

        $this->prepareCurrencySortIndex($currencyData);

        $groupedDataBody = [];
        $tmpMonthData = [];
        $rawCreditData[] = ['month' => 13];
        $currentMonth = current($rawCreditData)['month'];

        foreach ($rawCreditData as $item) {
            if ($item['month'] !== $currentMonth) {
                $groupedDataBody[$currentMonth] = $this->getTableBody($tmpMonthData);

                $tmpMonthData = [];
                $currentMonth = $item['month'];
            }

            $tmpMonthData[] = $item;
        }

        $groupedData['body'] = $groupedDataBody;

        return $groupedData;
    }

    private function init()
    {
        self::$typeSortIndex = [
            'regular' => 1,
            'irregular' => 2,
            'taxes' => 3,
            'exchange' => 4,
            self::$totalColumn => self::LAST_ELEMENT,
        ];
        self::$typeCount = count(self::$typeSortIndex);
        self::$totalColumns = [
            trim(CreditRepository::ALL_TOTAL_COLUMN, "'"),
            trim(CreditRepository::SELECTED_TOTAL_COLUMN, "'"),
        ];
    }

    private function prepareCurrencySortIndex(array $currencyData): void
    {
        foreach ($currencyData as $currency) {
            self::$currencySortIndex[$currency['name']] = (int) $currency['order'];
        }
    }

    private function getTableHead(): array
    {
        $head = array_flip(self::$typeSortIndex);
        ksort($head);

        return $head;
    }

    private function getTableBody(array $rawCreditData): array
    {
        $groupedDataBody = [];
        foreach ($rawCreditData as $item) {
            $curIndex = self::$currencySortIndex[$item['currency']];
            $typeIndex = self::$typeSortIndex[$item['type']] ?? self::$typeSortIndex[self::$totalColumn];

            if (!array_key_exists($curIndex, $groupedDataBody)) {
                $groupedDataBody[$curIndex] = array_combine(
                    array_values(self::$typeSortIndex),
                    array_fill(0, self::$typeCount, null)
                );
            }

            if (in_array($item['type'], self::$totalColumns)) {
                $groupedDataBody[$curIndex][$typeIndex][$item['type']] = [
                    'sum' => $item['sum'],
                    'currency' => $item['currency'],
                    'type' => $item['type'],
                ];
            } else {
                $groupedDataBody[$curIndex][$typeIndex] = [
                    'sum' => $item['sum'],
                    'currency' => $item['currency'],
                    'type' => $item['type'],
                ];
            }
        }

        array_walk($groupedDataBody, 'ksort');

        ksort($groupedDataBody);

        return $groupedDataBody;
    }
}
