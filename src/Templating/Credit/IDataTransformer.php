<?php
declare(strict_types=1);

namespace App\Templating\Credit;

interface IDataTransformer
{
    public function transform(array $rawCreditData, array $currencyData): array;

    public function transformSeveralMonth(array $rawCreditData, array $currencyData): array;
}
