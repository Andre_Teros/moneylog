<?php
declare(strict_types=1);

namespace App\Templating\Capital;

class DataAggregator implements IDataAggregator
{
    public function groupCapitalData(array $rawCapitalData, array $locationData): array
    {
        $groupedData = [];
        $locationData = array_combine($locationData, array_fill(0, count($locationData), []));

        foreach ($rawCapitalData as $capitalData) {
            if (!isset($groupedData[$capitalData['date']])) {
                $groupedData[$capitalData['date']] = $locationData;
            }

            $groupedData[$capitalData['date']][$capitalData['location']][] = [
                'sum' => $capitalData['sum'],
                'currency' => $capitalData['currency'],
            ];
        }

        return $groupedData;
    }
}
