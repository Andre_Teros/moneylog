<?php
declare(strict_types=1);

namespace App\Templating\Capital;

interface IDataAggregator
{
    public function groupCapitalData(array $capitalData, array $locationData): array;
}
