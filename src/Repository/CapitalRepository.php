<?php
declare(strict_types=1);

namespace App\Repository;

use App\Entity\Capital;
use Doctrine\DBAL\Driver\Connection;
use Doctrine\ORM\EntityManagerInterface;
use Latitude\QueryBuilder\Query\SelectQuery;
use Latitude\QueryBuilder\QueryFactory;
use function Latitude\QueryBuilder\alias;
use function Latitude\QueryBuilder\express;
use function Latitude\QueryBuilder\field;
use function Latitude\QueryBuilder\on;
use function Latitude\QueryBuilder\func;

class CapitalRepository
{
    private $qb;
    private $connection;
    private $entityManager;

    public function __construct(QueryFactory $qb, Connection $connection, EntityManagerInterface $entityManager)
    {
        $this->qb = $qb;
        $this->connection = $connection;
        $this->entityManager = $entityManager;
    }

    public function findByYear(int $userId, ?int $year = null): array
    {
        $year = $year ?? (int)date('Y');

        $query = $this->qb
            ->select(
                'capital.sum',
                'capital.date',
                alias('cur.name', 'currency'),
                alias('l.name', 'location')
            )
            ->from('capital')
            ->innerJoin(alias('currencies', 'cur'), on('cur.id', 'capital.currency_id'))
            ->innerJoin(alias('capital_locations', 'l'), on('l.id', 'capital.capital_location_id'))
            ->where(field('capital.user_id')->eq($userId))
            ->andWhere(field(func('YEAR', 'capital.date'))->eq($year))
            ->orderBy('capital.date')
            ->orderBy('capital.currency_id')
            ->compile();

        $stmt = $this->connection->prepare($query->sql());
        $stmt->execute($query->params());

        return $stmt->fetchAll();
    }

    public function findLastState(int $userId): array
    {
        $subQuery = $this->getLastStateDateQuery($userId);

        $query = $this->qb
            ->select(
                'capital.sum',
                alias('cur.name', 'currency'),
                alias('l.name', 'location')
            )
            ->from('capital')
            ->innerJoin(alias('currencies', 'cur'), on('cur.id', 'capital.currency_id'))
            ->innerJoin(alias('capital_locations', 'l'), on('l.id', 'capital.capital_location_id'))
            ->where(field('capital.user_id')->eq($userId))
            ->andWhere(field('capital.date')->eq(express('(%s)', $subQuery)))
            ->orderBy('capital.date')
            ->orderBy('capital.currency_id')
            ->compile();

        $stmt = $this->connection->prepare($query->sql());
        $stmt->execute($query->params());

        return $stmt->fetchAll();
    }

    public function findLastStateForUpdate(int $userId): array
    {
        $subQuery = $this->getLastStateDateQuery($userId);

        $query = $this->qb
            ->select(
                'capital.sum',
                alias('cur.id', 'currency'),
                alias('l.id', 'location')
            )
            ->from('capital')
            ->innerJoin(alias('currencies', 'cur'), on('cur.id', 'capital.currency_id'))
            ->innerJoin(alias('capital_locations', 'l'), on('l.id', 'capital.capital_location_id'))
            ->where(field('capital.user_id')->eq($userId))
            ->andWhere(field('capital.date')->eq(express('(%s)', $subQuery)))
            ->orderBy('capital.date')
            ->orderBy('capital.currency_id')
            ->compile();

        $stmt = $this->connection->prepare($query->sql());
        $stmt->execute($query->params());

        return $stmt->fetchAll();
    }

    public function findLastStateByCurrencies(int $userId): array
    {
        $subQuery = $this->getLastStateDateQuery($userId);

        $query = $this->qb
            ->select(
                alias(func('SUM', 'capital.sum'), 'sum'),
                alias('cur.name', 'currency')
            )
            ->from('capital')
            ->innerJoin(alias('currencies', 'cur'), on('cur.id', 'capital.currency_id'))
            ->where(field('capital.user_id')->eq($userId))
            ->andWhere(field('capital.date')->eq(express('(%s)', $subQuery)))
            ->groupBy('capital.currency_id')
            ->orderBy('cur.id')
            ->compile();

        $stmt = $this->connection->prepare($query->sql());
        $stmt->execute($query->params());

        return $stmt->fetchAll();
    }

    public function findLastStateDate(int $userId): ?\DateTime
    {
        $query = $this->getLastStateDateQuery($userId);
        $stmt = $this->connection->prepare($query->compile()->sql());
        $stmt->execute($query->compile()->params());
        $result = $stmt->fetchColumn();

        if ($result === false) {
            return null;
        }

        return new \DateTime($result);
    }

    public function findTodayItem(int $userId, int $currencyId, int $locationId): ?Capital
    {
        return $this->entityManager
            ->getRepository(Capital::class)
            ->findOneBy([
                'user_id' => $userId,
                'currency' => $currencyId,
                'capitalLocation' => $locationId,
                'date' => new \DateTime(),
            ]);
    }

    public function findTop(int $userId, int $limit): array
    {
        $dateRange = $this->findTopDateRange($userId, $limit);

        if (empty($dateRange)) {
            return [];
        }

        $sql = $this->qb
            ->select(
                'capital.sum',
                'capital.date',
                alias('cur.name', 'currency'),
                alias('l.name', 'location')
            )
            ->from('capital')
            ->innerJoin(alias('currencies', 'cur'), on('cur.id', 'capital.currency_id'))
            ->innerJoin(alias('capital_locations', 'l'), on('l.id', 'capital.capital_location_id'))
            ->where(field('capital.user_id')->eq($userId))
            ->orderBy('capital.date', 'DESC')
            ->orderBy('capital.currency_id');

        if (empty($dateRange['from'])) {
            $sql->andWhere(field('capital.date')->eq($dateRange['to']));
        } else {
            $sql->andWhere(field('capital.date')->between($dateRange['from'], $dateRange['to']));
        }

        $query = $sql->compile();
        $stmt = $this->connection->prepare($query->sql());
        $stmt->execute($query->params());

        return $stmt->fetchAll();
    }

    private function getLastStateDateQuery(int $userId): SelectQuery
    {
        // TODO: [think] order by date and limit 1 instead of group by
        return $this->qb
            ->select(func('MAX', 'date'))
            ->from('capital')
            ->where(field('user_id')->eq($userId))
            ->groupBy('user_id');
    }

    private function findTopDateRange(int $userId, int $limit): array
    {
        $query = $this->qb
            ->selectDistinct('date')
            ->from('capital')
            ->where(field('capital.user_id')->eq($userId))
            ->orderBy('date', 'DESC')
            ->limit($limit)
            ->compile();

        $stmt = $this->connection->prepare($query->sql());
        $stmt->execute($query->params());
        $result = [];
        while ($row = $stmt->fetchColumn()) {
            if (empty($result)) {
                $result['to'] = $row;
            } else {
                $result['from'] = $row;
            }
        }

        return $result;
    }
}
