<?php
declare(strict_types=1);

namespace App\Repository;

use Doctrine\DBAL\DBALException;
use Doctrine\DBAL\Driver\Connection;
use Latitude\QueryBuilder\QueryFactory;
use function Latitude\QueryBuilder\alias;
use function Latitude\QueryBuilder\field;

class PlanRepository
{
    private $qb;
    private $connection;

    public function __construct(QueryFactory $qb, Connection $connection)
    {
        $this->qb = $qb;
        $this->connection = $connection;
    }

    /**
     * @param int $userId
     * @param int $year
     *
     * @return array|false
     */
    public function find(int $userId, int $year)
    {
        $query = $this->qb
            ->select(
                'p.overvalue',
                'p.month_aims'
            )
            ->from(alias('plans', 'p'))
            ->where(field('p.user_id')->eq($userId))
            ->andWhere(field('p.year')->eq($year))
            ->compile();

        $stmt = $this->connection->prepare($query->sql());
        $stmt->execute($query->params());

        return $stmt->fetch();
    }

    /**
     * @param int $userId
     * @param int $year
     * @param int|null $overvalue
     * @param string|null $monthAims
     *
     * @return bool
     * @throws DBALException
     */
    public function insert(int $userId, int $year, ?int $overvalue = null, ?string $monthAims = null): bool
    {
        $monthAims = $monthAims ?? '{}';

        $query = $this->qb
            ->insert('plans')
            ->columns('year', 'user_id', 'overvalue', 'month_aims')
            ->values($year, $userId, $overvalue, $monthAims)
            ->compile();

        $stmt = $this->connection->prepare($query->sql());

        return $stmt->execute($query->params());
    }

    /**
     * @param int $userId
     * @param int $year
     * @param int|null $overvalue
     * @param string|null $monthAims
     *
     * @return bool
     * @throws DBALException
     */
    public function update(int $userId, int $year, ?int $overvalue = null, ?string $monthAims = null): bool
    {
        $updates = [];

        if ($overvalue !== null) {
            $updates['overvalue'] = $overvalue;
        }

        if ($monthAims !== null) {
            $updates['month_aims'] = $monthAims;
        }

        if (empty($updates)) {
            return true;
        }

        $query = $this->qb
            ->update('plans')
            ->where(field('plans.user_id')->eq($userId))
            ->andWhere(field('plans.year')->eq($year))
            ->set($updates)
            ->compile();

        $stmt = $this->connection->prepare($query->sql());

        return $stmt->execute($query->params());
    }

    /**
     * @param int $userId
     *
     * @return array|false
     */
    public function getAllPlans(int $userId)
    {
        $query = $this->qb
            ->select(
                'p.overvalue',
                'p.month_aims',
                'p.year'
            )
            ->from(alias('plans', 'p'))
            ->where(field('p.user_id')->eq($userId))
            ->compile();

        $stmt = $this->connection->prepare($query->sql());
        $stmt->execute($query->params());

        return $stmt->fetchAll();
    }
}
