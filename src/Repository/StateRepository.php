<?php
declare(strict_types=1);

namespace App\Repository;

use App\Entity\Currency;
use App\Entity\State;
use Doctrine\DBAL\Driver\Connection;
use Doctrine\ORM\EntityManagerInterface;
use Latitude\QueryBuilder\QueryFactory;
use function Latitude\QueryBuilder\alias;
use function Latitude\QueryBuilder\field;
use function Latitude\QueryBuilder\on;

class StateRepository
{
    private $qb;
    private $connection;
    private $entityManager;

    public function __construct(QueryFactory $qb, Connection $connection, EntityManagerInterface $entityManager)
    {
        $this->qb = $qb;
        $this->connection = $connection;
        $this->entityManager = $entityManager;
    }

    public function findByUser(int $userId): array
    {
        $query = $this->qb
            ->select(
                'states.sum',
                alias('currencies.name', 'currency'),
                alias('currencies.sign', 'currency_sign')
            )
            ->from('states')
            ->innerJoin('currencies', on('states.currency_id', 'currencies.id'))
            ->where(field('states.user_id')->eq($userId))
            ->orderBy('states.currency_id')
            ->compile();

        $stmt = $this->connection->prepare($query->sql());
        $stmt->execute($query->params());

        return $stmt->fetchAll();
    }

    public function getByUserAndCurrency(int $userId, Currency $currency): State
    {
        $state = $this->entityManager->getRepository(State::class)->findOneBy([
            'user_id' => $userId,
            'currency' => $currency,
        ]);

        if ($state === null) {
            $state = State::getZeroState($currency, $userId);
        }

        return $state;
    }
}
