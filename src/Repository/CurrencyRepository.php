<?php
declare(strict_types=1);

namespace App\Repository;

use App\Entity\Currency;
use App\Exceptions\NoDataFound;
use App\Exceptions\NoEntityFound;
use Doctrine\DBAL\Driver\Connection;
use Doctrine\ORM\EntityManagerInterface;
use Latitude\QueryBuilder\QueryFactory;

class CurrencyRepository
{
    private $qb;
    private $connection;
    private $entityManager;

    public function __construct(QueryFactory $qb, Connection $connection, EntityManagerInterface $entityManager)
    {
        $this->qb = $qb;
        $this->connection = $connection;
        $this->entityManager = $entityManager;
    }

    /**
     * @throws NoDataFound
     */
    public function getAll(?array $field = null): array
    {
        $field = $field ?? ['id', 'name', 'sign'];

        $query = $this->qb
            ->select(...$field)
            ->from('currencies')
            ->orderBy('id')
            ->compile();

        $stmt = $this->connection->prepare($query->sql());
        $stmt->execute();

        $result = $stmt->fetchAll();
        if (empty($result)) {
            throw new NoDataFound(__METHOD__, [], $query->sql());
        }

        return $result;
    }

    public function find(int $id): ?Currency
    {
        return $this->entityManager->getRepository(Currency::class)->find($id);
    }

    public function findOneBy(array $criteria): ?Currency
    {
        return $this->entityManager->getRepository(Currency::class)->findOneBy($criteria);
    }

    /**
     * @throws NoEntityFound
     */
    public function get(int $id): Currency
    {
        $currency = $this->find($id);
        if ($currency === null) {
            throw new NoEntityFound($id, Currency::class);
        }

        return $currency;
    }
}
