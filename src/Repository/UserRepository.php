<?php
declare(strict_types=1);

namespace App\Repository;

use App\Entity\User;
use Doctrine\Common\Persistence\ObjectRepository;
use Doctrine\ORM\EntityManagerInterface;

class UserRepository
{
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function find(int $id): ?User
    {
        return $this->getRepository()->find($id);
    }

    private function getRepository(): ObjectRepository
    {
        return $this->entityManager->getRepository(User::class);
    }
}
