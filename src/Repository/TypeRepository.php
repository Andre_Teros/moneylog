<?php
declare(strict_types=1);

namespace App\Repository;

use App\Entity\Type;
use App\Exceptions\NoDataFound;
use App\Exceptions\NoEntityFound;
use Doctrine\DBAL\Driver\Connection;
use Doctrine\ORM\EntityManagerInterface;
use Latitude\QueryBuilder\QueryFactory;
use function Latitude\QueryBuilder\field;

class TypeRepository
{
    private $qb;
    private $connection;
    private $entityManager;

    public function __construct(QueryFactory $qb, Connection $connection, EntityManagerInterface $entityManager)
    {
        $this->qb = $qb;
        $this->connection = $connection;
        $this->entityManager = $entityManager;
    }

    /**
     * @throws NoDataFound
     */
    public function getAllBelongsTo(int $belongsTo): array
    {
        $query = $this->qb
            ->select('id', 'name')
            ->from('types')
            ->where(field('belongs_to')->eq($belongsTo))
            ->compile();

        $stmt = $this->connection->prepare($query->sql());
        $stmt->execute($query->params());

        $result = $stmt->fetchAll();
        if (empty($result)) {
            throw new NoDataFound(__METHOD__, [$belongsTo], $query->sql(), $query->params());
        }

        return $result;
    }

    public function find(int $id): ?Type
    {
        return $this->entityManager->getRepository(Type::class)->find($id);
    }

    public function findOneBy(array $criteria): ?Type
    {
        return $this->entityManager->getRepository(Type::class)->findOneBy($criteria);
    }

    /**
     * @throws NoEntityFound
     */
    public function get(int $id, ?int $belongsTo = null): Type
    {
        if ($belongsTo === null) {
            $type = $this->find($id);
        } else {
            $type = $this->findOneBy([
                'id' => $id,
                'belongsTo' => $belongsTo,
            ]);
        }

        if ($type === null) {
            throw new NoEntityFound($id, Type::class);
        }

        return $type;
    }
}
