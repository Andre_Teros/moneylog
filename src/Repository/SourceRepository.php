<?php
declare(strict_types=1);

namespace App\Repository;

use App\Entity\Source;
use App\Exceptions\NoDataFound;
use App\Exceptions\NoEntityFound;
use Doctrine\DBAL\Driver\Connection;
use Doctrine\ORM\EntityManagerInterface;
use Latitude\QueryBuilder\QueryFactory;
use function Latitude\QueryBuilder\field;

class SourceRepository
{
    private $qb;
    private $connection;
    private $entityManager;

    public function __construct(QueryFactory $qb, Connection $connection, EntityManagerInterface $entityManager)
    {
        $this->qb = $qb;
        $this->connection = $connection;
        $this->entityManager = $entityManager;
    }

    /**
     * @throws NoDataFound
     */
    public function getAll(int $userId): array
    {
        $query = $this->qb
            ->select('id', 'name')
            ->from('sources')
            ->where(field('user_id')->eq($userId))
            ->compile();

        $stmt = $this->connection->prepare($query->sql());
        $stmt->execute($query->params());

        $result = $stmt->fetchAll();
        if (empty($result)) {
            throw new NoDataFound(__METHOD__, [$userId], $query->sql(), $query->params());
        }

        return $result;
    }

    public function find(int $id): ?Source
    {
        return $this->entityManager->getRepository(Source::class)->find($id);
    }

    public function findOneBy(array $criteria): ?Source
    {
        return $this->entityManager->getRepository(Source::class)->findOneBy($criteria);
    }

    /**
     * @throws NoEntityFound
     */
    public function get(int $id): Source
    {
        $source = $this->find($id);
        if ($source === null) {
            throw new NoEntityFound($id, Source::class);
        }

        return $source;
    }
}
