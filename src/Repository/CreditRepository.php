<?php
declare(strict_types=1);

namespace App\Repository;

use App\Entity\Credit;
use App\Exceptions\NoEntityFound;
use Doctrine\DBAL\Driver\Connection;
use Doctrine\ORM\EntityManagerInterface;
use Latitude\QueryBuilder\QueryFactory;
use function Latitude\QueryBuilder\alias;
use function Latitude\QueryBuilder\field;
use function Latitude\QueryBuilder\literal;
use function Latitude\QueryBuilder\on;
use function Latitude\QueryBuilder\func;

class CreditRepository
{
    public const ALL_TOTAL_COLUMN = "'all_total'";
    public const SELECTED_TOTAL_COLUMN = "'selected_total'";

    private $qb;
    private $connection;
    private $entityManager;

    public function __construct(QueryFactory $qb, Connection $connection, EntityManagerInterface $entityManager)
    {
        $this->qb = $qb;
        $this->connection = $connection;
        $this->entityManager = $entityManager;
    }

    public function findMonthly(int $userId, ?int $month = null, ?int $year = null): array
    {
        $month = $month ?? (int)date('m');
        $year = $year ?? (int)date('Y');

        $query = $this->qb
            ->select(
                'c.id',
                'c.sum',
                alias('cur.name', 'currency_name'),
                alias('cur.sign', 'currency_sign'),
                alias('t.name', 'type'),
                'c.tag',
                'c.description',
                'c.date'
            )
            ->from(alias('credits', 'c'))
            ->innerJoin(alias('currencies', 'cur'), on('cur.id', 'c.currency_id'))
            ->innerJoin(alias('types', 't'), on('t.id', 'c.type_id'))
            ->where(field('c.user_id')->eq($userId))
            ->andWhere(field(func('MONTH', 'c.date'))->eq($month))
            ->andWhere(field(func('YEAR', 'c.date'))->eq($year))
            ->orderBy('c.type_id')
            ->orderBy('c.tag')
            ->orderBy('cur.id')
            ->orderBy('c.date', 'DESC')
            ->orderBy('c.created', 'DESC')
            ->compile();

        $stmt = $this->connection->prepare($query->sql());
        $stmt->execute($query->params());

        return $stmt->fetchAll();
    }

    public function findMonthlyGrouped(int $userId, ?int $month = null, ?int $year = null): array
    {
        // TODO: user sort index
        $month = $month ?? (int)date('m');
        $year = $year ?? (int)date('Y');

        $query = $this->qb
            ->select(
                alias(func('SUM', 'c.sum'), 'sum'),
                alias('cur.name', 'currency_name'),
                alias('cur.sign', 'currency_sign'),
                alias('t.name', 'type'),
                'c.tag'
            )
            ->from(alias('credits', 'c'))
            ->innerJoin(alias('currencies', 'cur'), on('cur.id', 'c.currency_id'))
            ->innerJoin(alias('types', 't'), on('t.id', 'c.type_id'))
            ->where(field('c.user_id')->eq($userId))
            ->andWhere(field(func('MONTH', 'c.date'))->eq($month))
            ->andWhere(field(func('YEAR', 'c.date'))->eq($year))
            ->groupBy('cur.id', 'c.type_id', 'c.tag')
            ->orderBy('c.type_id')
            ->orderBy('cur.id')
            ->orderBy(func('MIN', 'c.created'))
            ->orderBy('c.tag')
            ->compile();

        $stmt = $this->connection->prepare($query->sql());
        $stmt->execute($query->params());

        return $stmt->fetchAll();
    }

    public function findSummaryCurrentMonth(int $userId): array
    {
        // TODO: take from argument
        $selectedTypes = [1,2];

        $queryTotalEachType = $this->qb
            ->select(
                alias(func('SUM', 'c.sum'), 'sum'),
                alias('cur.name', 'currency'),
                alias('t.name', 'type')
            )
            ->from(alias('credits', 'c'))
            ->innerJoin(alias('currencies', 'cur'), on('cur.id', 'c.currency_id'))
            ->innerJoin(alias('types', 't'), on('t.id', 'c.type_id'))
            ->where(field('c.user_id')->eq($userId))
            ->andWhere(field(func('MONTH', 'c.date'))->eq((int)date('m')))
            ->andWhere(field(func('YEAR', 'c.date'))->eq((int)date('Y')))
            ->groupBy('currency', 'type');

        $queryTotalSelectedTypes = $this->qb
            ->select(
                alias(func('SUM', 'c.sum'), 'sum'),
                alias('cur.name', 'currency'),
                alias(literal(self::SELECTED_TOTAL_COLUMN), 'type')
            )
            ->from(alias('credits', 'c'))
            ->innerJoin(alias('currencies', 'cur'), on('cur.id', 'c.currency_id'))
            ->where(field('c.user_id')->eq($userId))
            ->andWhere(field(func('MONTH', 'c.date'))->eq((int)date('m')))
            ->andWhere(field(func('YEAR', 'c.date'))->eq((int)date('Y')))
            ->andWhere(field('c.type_id')->in(...$selectedTypes))
            ->groupBy('currency');

        $queryTotalAllTypes = $this->qb
            ->select(
                alias(func('SUM', 'c.sum'), 'sum'),
                alias('cur.name', 'currency'),
                alias(literal(self::ALL_TOTAL_COLUMN), 'type')
            )
            ->from(alias('credits', 'c'))
            ->innerJoin(alias('currencies', 'cur'), on('cur.id', 'c.currency_id'))
            ->where(field('c.user_id')->eq($userId))
            ->andWhere(field(func('MONTH', 'c.date'))->eq((int)date('m')))
            ->andWhere(field(func('YEAR', 'c.date'))->eq((int)date('Y')))
            ->groupBy('currency');

        $query = $queryTotalEachType
            ->union($queryTotalSelectedTypes)
            ->union($queryTotalAllTypes)
            ->orderBy('type')
            ->orderBy('currency')
            ->compile();

        $stmt = $this->connection->prepare($query->sql());
        $stmt->execute($query->params());

        return $stmt->fetchAll();
    }

    public function findTagsByUser(int $userId): array
    {
        $dateFrom = new \DateTime('first day of -1 months');
        $dateTo = new \DateTime('now');

        $query = $this->qb
            ->selectDistinct(
                'c.tag'
            )
            ->from(alias('credits', 'c'))
            ->where(field('c.user_id')->eq($userId))
            ->andWhere(field('c.date')->between($dateFrom->format('Y-m-d'), $dateTo->format('Y-m-d')))
            ->compile();

        $stmt = $this->connection->prepare($query->sql());
        $stmt->execute($query->params());

        return $stmt->fetchAll(\PDO::FETCH_COLUMN);
    }

    public function find(int $id): ?Credit
    {
        return $this->entityManager->getRepository(Credit::class)->find($id);
    }

    /**
     * @throws NoEntityFound
     */
    public function get(int $id): Credit
    {
        $credit = $this->find($id);
        if ($credit === null) {
            throw new NoEntityFound($id, Credit::class);
        }

        return $credit;
    }

    public function findMonthlyTotalForYear(int $userId, ?int $year = null): array
    {
        // TODO: take from argument
        $selectedTypes = [1,2];
        $year = $year ?? (int)date('Y');

        $queryTotalEachType = $this->qb
            ->select(
                alias(func('SUM', 'c.sum'), 'sum'),
                alias('cur.name', 'currency'),
                alias('t.name', 'type'),
                alias(func('MONTH', 'c.date'), 'month')
            )
            ->from(alias('credits', 'c'))
            ->innerJoin(alias('currencies', 'cur'), on('cur.id', 'c.currency_id'))
            ->innerJoin(alias('types', 't'), on('t.id', 'c.type_id'))
            ->where(field('c.user_id')->eq($userId))
            ->andWhere(field(func('YEAR', 'c.date'))->eq($year))
            ->groupBy('currency', 'type', func('MONTH', 'c.date'));

        $queryTotalSelectedTypes = $this->qb
            ->select(
                alias(func('SUM', 'c.sum'), 'sum'),
                alias('cur.name', 'currency'),
                alias(literal(self::SELECTED_TOTAL_COLUMN), 'type'),
                alias(func('MONTH', 'c.date'), 'month')
            )
            ->from(alias('credits', 'c'))
            ->innerJoin(alias('currencies', 'cur'), on('cur.id', 'c.currency_id'))
            ->innerJoin(alias('types', 't'), on('t.id', 'c.type_id'))
            ->where(field('c.user_id')->eq($userId))
            ->andWhere(field('c.type_id')->in(...$selectedTypes))
            ->andWhere(field(func('YEAR', 'c.date'))->eq($year))
            ->groupBy('currency', 'type', func('MONTH', 'c.date'));

        $queryTotalAllTypes = $this->qb
            ->select(
                alias(func('SUM', 'c.sum'), 'sum'),
                alias('cur.name', 'currency'),
                alias(literal(self::ALL_TOTAL_COLUMN), 'type'),
                alias(func('MONTH', 'c.date'), 'month')
            )
            ->from(alias('credits', 'c'))
            ->innerJoin(alias('currencies', 'cur'), on('cur.id', 'c.currency_id'))
            ->innerJoin(alias('types', 't'), on('t.id', 'c.type_id'))
            ->where(field('c.user_id')->eq($userId))
            ->andWhere(field(func('YEAR', 'c.date'))->eq($year))
            ->groupBy('currency', 'type', func('MONTH', 'c.date'));

        $query = $queryTotalEachType
            ->union($queryTotalSelectedTypes)
            ->union($queryTotalAllTypes)
            ->orderBy('month')
            ->orderBy('type')
            ->orderBy('currency')
            ->compile();

        $stmt = $this->connection->prepare($query->sql());
        $stmt->execute($query->params());

        return $stmt->fetchAll();
    }

    public function findYears(int $userId): array
    {
        $query = $this->qb
            ->selectDistinct(alias(func('YEAR', 'date'), 'year'))
            ->from('credits')
            ->where(field('user_id')->eq($userId))
            ->orderBy('year', 'DESC')
            ->compile();

        $stmt = $this->connection->prepare($query->sql());
        $stmt->execute($query->params());

        return $stmt->fetchAll(\PDO::FETCH_COLUMN);
    }
}
