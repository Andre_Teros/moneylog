<?php
declare(strict_types=1);

namespace App\Repository;

use App\Entity\Debit;
use App\Exceptions\NoEntityFound;
use Doctrine\DBAL\Driver\Connection;
use Doctrine\ORM\EntityManagerInterface;
use Latitude\QueryBuilder\QueryFactory;
use function Latitude\QueryBuilder\alias;
use function Latitude\QueryBuilder\field;
use function Latitude\QueryBuilder\on;
use function Latitude\QueryBuilder\func;

class DebitRepository
{
    private $qb;
    private $connection;
    private $entityManager;

    public function __construct(QueryFactory $qb, Connection $connection, EntityManagerInterface $entityManager)
    {
        $this->qb = $qb;
        $this->connection = $connection;
        $this->entityManager = $entityManager;
    }

    public function findMonthly(int $userId, ?int $month = null, ?int $year = null): array
    {
        $month = $month ?? (int)date('m');
        $year = $year ?? (int)date('Y');

        $query = $this->qb
            ->select(
                'd.id',
                'd.sum',
                alias('cur.name', 'currency'),
                alias('t.name', 'type'),
                alias('s.name', 'source'),
                'd.date'
            )
            ->from(alias('debits', 'd'))
            ->innerJoin(alias('currencies', 'cur'), on('cur.id', 'd.currency_id'))
            ->innerJoin(alias('types', 't'), on('t.id', 'd.type_id'))
            ->innerJoin(alias('sources', 's'), on('s.id', 'd.source_id'))
            ->where(field('d.user_id')->eq($userId))
            ->andWhere(field(func('MONTH', 'd.date'))->eq($month))
            ->andWhere(field(func('YEAR', 'd.date'))->eq($year))
            ->orderBy('d.date')
            ->compile();

        $stmt = $this->connection->prepare($query->sql());
        $stmt->execute($query->params());

        return $stmt->fetchAll();
    }

    /**
     * @throws NoEntityFound
     */
    public function get(int $id): Debit
    {
        $debit = $this->entityManager->getRepository(Debit::class)->find($id);
        if ($debit === null) {
            throw new NoEntityFound($id, Debit::class);
        }

        return $debit;
    }
}
