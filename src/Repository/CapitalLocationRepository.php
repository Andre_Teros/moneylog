<?php
declare(strict_types=1);

namespace App\Repository;

use App\Entity\CapitalLocation;
use App\Exceptions\NoDataFound;
use Doctrine\DBAL\Driver\Connection;
use Doctrine\DBAL\FetchMode;
use Doctrine\ORM\EntityManagerInterface;
use Latitude\QueryBuilder\QueryFactory;
use function Latitude\QueryBuilder\field;

class CapitalLocationRepository
{
    private $qb;
    private $connection;
    private $entityManager;

    public function __construct(QueryFactory $qb, Connection $connection, EntityManagerInterface $entityManager)
    {
        $this->qb = $qb;
        $this->connection = $connection;
        $this->entityManager = $entityManager;
    }

    /**
     * @throws NoDataFound
     */
    public function getAll(int $userId, ?array $fields = null): array
    {
        $fields = $fields ?? ['id', 'name'];
        $fetchMode = count($fields) === 1 ? FetchMode::COLUMN : null;
        $query = $this->qb
            ->select(...$fields)
            ->from('capital_locations')
            ->where(field('user_id')->eq($userId))
            ->orderBy('id')
            ->compile();

        $stmt = $this->connection->prepare($query->sql());
        $stmt->execute($query->params());

        $result = $stmt->fetchAll($fetchMode);
        if (empty($result)) {
            throw new NoDataFound(__METHOD__, [], $query->sql());
        }

        return $result;
    }

    public function find(int $id): ?CapitalLocation
    {
        return $this->entityManager->getRepository(CapitalLocation::class)->find($id);
    }
}
