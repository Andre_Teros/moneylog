<?php
declare(strict_types=1);

namespace App\Twig;

use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;

class Filters extends AbstractExtension
{
    public function getFilters(): array
    {
        return [
            new TwigFilter('c_number_format', [$this, 'customNumberFormat']),
            new TwigFilter('brace_break', [$this, 'braceBreak'], ['is_safe' => ['html']]),
        ];
    }

    // TODO: get settings from config
    public function customNumberFormat($number): string
    {
        $number = (float)$number;

        if ($number - round($number) === 0.0) {
            return number_format($number, 0, '.', "'");
        }

        return number_format($number, 2, '.', "'");
    }

    public function braceBreak($string): string
    {
        return str_replace('(', '<wbr>(', $string);
    }
}
