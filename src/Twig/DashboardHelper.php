<?php
declare(strict_types=1);

namespace App\Twig;

use App\Templating\Credit\TableDataTransformer;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

class DashboardHelper extends AbstractExtension
{
    public function getFunctions(): array
    {
        return [
            new TwigFunction('is_last', [$this, 'isLastColumn']),
        ];
    }

    public function isLastColumn($columnName): bool
    {
        return $columnName === TableDataTransformer::LAST_ELEMENT;
    }
}
