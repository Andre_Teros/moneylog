<?php
declare(strict_types=1);

namespace App\Twig;

use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

class SmartLink extends AbstractExtension
{
    public function getFunctions(): array
    {
        return [
            new TwigFunction('build_query', [$this, 'buildQuery']),
            new TwigFunction('build_month_link', [$this, 'buildMonthLink']),
        ];
    }

    public function buildQuery(string $link, array $parameters = []): string
    {
        if (!empty($parameters)) {
            $link .= '?' . http_build_query($parameters);
        }

        return $link;
    }

    public function buildMonthLink(string $link, int $month, int $year): string
    {
        $month = (string)$month;
        $year = (string)$year;

        if (mb_strlen($month) === 1) {
            $month = "0{$month}";
        }

        return "{$link}/{$month}-{$year}";
    }
}
