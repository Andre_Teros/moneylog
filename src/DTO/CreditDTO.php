<?php
declare(strict_types=1);

namespace App\DTO;

use App\Service\Validator\ConstraintValues as V;
use Symfony\Component\Validator\Constraints as Assert;

class CreditDTO
{
    /**
     * @Assert\NotBlank()
     * @Assert\Regex(pattern = V::FLOAT)
     */
    public $sum;

    /**
     * @Assert\NotBlank()
     * @Assert\Regex(pattern = V::ID)
     */
    public $currency;

    /**
     * @Assert\NotBlank()
     * @Assert\Regex(pattern = V::ID)
     */
    public $type;

    /**
     * @Assert\NotBlank()
     * @Assert\Regex(pattern = V::WORD)
     * @Assert\Length(min="2", max="50")
     */
    public $tag;

    /**
     * @Assert\Date()
     */
    public $date;

    /**
     * @Assert\Length(max="200")
     */
    public $description;

    public static function fromRequest(array $requestData): CreditDTO
    {
        $dto = new self();

        $dto->sum = isset($requestData['sum']) ? trim($requestData['sum']) : null;
        $dto->currency = $requestData['currency'] ?? null;
        $dto->type = $requestData['type'] ?? null;
        $dto->tag = isset($requestData['tag']) ? trim($requestData['tag']) : null;
        $dto->date = $requestData['date'] ?? null;
        $description = isset($requestData['description']) ? trim($requestData['description']) : null;
        $dto->description = !empty($description) ? strip_tags($description) : null;

        return $dto;
    }
}
