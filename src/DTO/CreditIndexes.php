<?php
declare(strict_types=1);

namespace App\DTO;

class CreditIndexes
{
    /** @var int */
    private $typeCounter = 0;
    /** @var int */
    public $sum;
    /** @var int */
    public $tag;

    public function __construct(int $sum, int $tag)
    {
        $this->sum = $sum;
        $this->tag = $tag;
    }

    public function getNextCounter(): int
    {
        return $this->typeCounter++;
    }

    public static function fromIndex(int $index): CreditIndexes
    {
        return new static($index * 2, $index * 2 + 1);
    }
}
