<?php
declare(strict_types=1);

namespace App\DTO;

use App\Service\Validator\ConstraintValues as V;
use Symfony\Component\Validator\Constraints as Assert;

class DebitDTO
{
    public const GROUP_ID = 'id';
    public const GROUP_NAME = 'name';

    /**
     * @Assert\NotBlank()
     * @Assert\Regex(pattern = V::FLOAT)
     */
    public $sum;

    /**
     * @Assert\NotBlank()
     * @Assert\Regex(pattern = V::ID)
     */
    public $currency;

    /**
     * @Assert\NotBlank()
     * @Assert\Regex(pattern = V::ID)
     */
    public $type;

    /**
     * @Assert\NotBlank()
     * @Assert\Regex(pattern = V::ID, groups={DebitDTO::GROUP_ID})
     * @Assert\Regex(pattern = V::WORD, groups={DebitDTO::GROUP_NAME})
     * @Assert\Length(min="2", max="50", groups={DebitDTO::GROUP_NAME})
     */
    public $source;

    public $group;

    /**
     * @Assert\Date()
     */
    public $date;

    /**
     * @Assert\Length(max="200")
     */
    public $description;

    public static function fromRequest(array $requestData): DebitDTO
    {
        $dto = new self();

        $dto->sum = isset($requestData['sum']) ? trim($requestData['sum']) : null;
        $dto->currency = $requestData['currency'] ?? null;
        $dto->type = $requestData['type'] ?? null;
        $dto->source = isset($requestData['source']) ? trim($requestData['source']) : null;
        $dto->date = $requestData['date'] ?? null;
        $description = isset($requestData['description']) ? trim($requestData['description']) : null;
        $dto->description = !empty($description) ? strip_tags($description) : null;

        return $dto;
    }
}
