<?php
declare(strict_types=1);

namespace App\DTO;

class CreditUIGrouped
{
    public $groupedData = [];
    public $metaData = [];
    public $nestedHeaders;
    public $colHeaders;
    public $colWidths;

    public function __construct($groupedData, $metaData, $nestedHeaders, $colHeaders, $colWidths)
    {
        $this->groupedData = $groupedData;
        $this->metaData = $metaData;
        $this->nestedHeaders = $nestedHeaders;
        $this->colHeaders = $colHeaders;
        $this->colWidths = $colWidths;
    }
}
