<?php
declare(strict_types=1);

namespace App\DTO;

use App\Service\Validator\ConstraintValues as V;
use Symfony\Component\Validator\Constraints as Assert;

class DebtDTO
{
    public const GROUP_ID = 'id';
    public const GROUP_NAME = 'name';

    /**
     * @Assert\NotBlank()
     * @Assert\Regex(pattern = V::FLOAT)
     */
    public $sum;

    /**
     * @Assert\NotBlank()
     * @Assert\Length(max="200")
     */
    public $debtor;

    /**
     * @Assert\NotBlank()
     * @Assert\Regex(pattern = V::ID)
     */
    public $currency;

    /**
     * @Assert\Date()
     */
    public $date;

    public static function fromRequest(array $requestData): DebtDTO
    {
        $dto = new self();

        $dto->sum = isset($requestData['sum']) ? trim($requestData['sum']) : null;
        $dto->debtor = $requestData['debtor'] ?? null;
        $dto->currency = $requestData['currency'] ?? null;
        $dto->date = $requestData['date'] ?? null;

        return $dto;
    }
}
