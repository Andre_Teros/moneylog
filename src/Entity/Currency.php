<?php
declare(strict_types=1);

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="currencies")
 */
class Currency
{
    public const DEFAULT = 'UAH';

    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;
    /**
     * @ORM\Column(type="string")
     */
    private $name;

    /**
     * @ORM\Column(type="string")
     */
    private $sign;

    public function getId(): int
    {
        return $this->id;
    }

    public function getSign(): string
    {
        return $this->sign;
    }
}
