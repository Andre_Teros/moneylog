<?php
declare(strict_types=1);

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="states")
 */
class State
{
    public const DECREASE = 0;
    public const INCREASE = 1;

    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;
    /**
     * @ORM\Column(type="decimal")
     */
    private $sum;
    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Currency")
     * @ORM\JoinColumn(nullable=false)
     */
    private $currency;
    /**
     * @ORM\Column(type="integer")
     */
    private $user_id;

    public function __construct(
        float $sum,
        Currency $currency,
        int $user_id
    ) {
        $this->sum = $sum;
        $this->currency = $currency;
        $this->user_id = $user_id;
    }

    public function changeSum(float $sum, int $option): void
    {
        if ($option === self::DECREASE) {
            $this->sum -= $sum;
        } else {
            $this->sum += $sum;
        }
    }

    public static function getZeroState(Currency $currency, int $user_id): State
    {
        return new self(0, $currency, $user_id);
    }
}
