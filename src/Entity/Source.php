<?php
declare(strict_types=1);

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="sources")
 */
class Source
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;
    /**
     * @ORM\Column(type="string")
     */
    private $name;
    /**
     * @ORM\Column(type="integer")
     */
    private $user_id;
    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $description;

    public function __construct(string $name, int $user_id, ?string $description = null)
    {
        $this->name = $name;
        $this->user_id = $user_id;
        $this->description = $description;
    }

    public function getId(): int
    {
        return $this->id;
    }
}
