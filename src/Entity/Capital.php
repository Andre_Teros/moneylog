<?php
declare(strict_types=1);

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="capital")
 */
class Capital
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;
    /**
     * @ORM\Column(type="decimal")
     */
    private $sum;
    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Currency")
     * @ORM\JoinColumn(nullable=false)
     */
    private $currency;
    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\CapitalLocation")
     * @ORM\JoinColumn(nullable=false, fieldName="capital_location_id")
     */
    private $capitalLocation;
    /**
     * @ORM\Column(type="integer")
     */
    private $user_id;
    /**
     * @ORM\Column(type="date")
     */
    private $date;

    public function __construct(
        float $sum,
        Currency $currency,
        CapitalLocation $capitalLocation,
        int $user_id,
        ?\DateTime $date = null
    ) {
        $this->sum = $sum;
        $this->currency = $currency;
        $this->capitalLocation = $capitalLocation;
        $this->user_id = $user_id;
        $this->date = $date ?? new \DateTime();
    }

    public function setSum(float $sum): void
    {
        $this->sum = $sum;
    }
}
