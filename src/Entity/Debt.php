<?php
declare(strict_types=1);

namespace App\Entity;

use App\Event\DebtAdded;
use App\Event\DebtRepaid;
use App\Event\EventTrait;
use App\Event\IDomainEvent;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="debts")
 */
class Debt implements IDomainEvent
{
    use EventTrait;

    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;
    /**
     * @ORM\Column(type="float")
     */
    private $sum;
    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Currency", fetch="EAGER")
     * @ORM\JoinColumn(nullable=false)
     */
    private $currency;
    /**
     * @ORM\Column(type="integer")
     */
    private $user_id;
    /**
     * @ORM\Column(type="string")
     */
    private $debtor;
    /**
     * @ORM\Column(type="boolean")
     */
    private $repaid = false;
    /**
     * @ORM\Column(type="date")
     */
    private $date;
    /**
     * @ORM\Column(type="date")
     */
    private $return_date;

    public function __construct(
        float $sum,
        Currency $currency,
        int $user_id,
        string $debtor,
        ?\DateTime $date = null
    ) {
        $this->sum = $sum;
        $this->currency = $currency;
        $this->user_id = $user_id;
        $this->debtor = $debtor;
        $this->date = $date ?? new \DateTime();

        $this->registerEvent(new DebtAdded($sum, $currency, $user_id));
    }

    public function repay($mark = false): void
    {
        if ($this->repaid === true) {
            return;
        }

        $this->repaid = true;
        $this->return_date = new \DateTime();

        if ($mark === false) {
            $this->registerEvent(new DebtRepaid($this->sum, $this->currency, $this->user_id));
        }
    }

    public function getId()
    {
        return $this->id;
    }

    public function getSum(): float
    {
        return $this->sum;
    }

    public function getCurrency(): Currency
    {
        return $this->currency;
    }

    public function getDebtor(): string
    {
        return $this->debtor;
    }

    public function isRepaid(): bool
    {
        return $this->repaid;
    }

    public function getDate(): \DateTime
    {
        return $this->date;
    }

    public function getReturnDate(): ?\DateTime
    {
        return $this->return_date;
    }
}
