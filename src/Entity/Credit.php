<?php
declare(strict_types=1);

namespace App\Entity;

use App\Event\CreditAdded;
use App\Event\CreditDeleted;
use App\Event\EventTrait;
use App\Event\IDomainEvent;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks
 * @ORM\Table(name="credits")
 */
class Credit implements IDomainEvent
{
    use EventTrait;

    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;
    /**
     * @ORM\Column(type="float")
     */
    private $sum;
    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Currency")
     * @ORM\JoinColumn(nullable=false)
     */
    private $currency;
    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Type")
     * @ORM\JoinColumn(nullable=false)
     */
    private $type;
    /**
     * @ORM\Column(type="integer")
     */
    private $user_id;
    /**
     * @ORM\Column(type="string")
     */
    private $tag;
    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $description;
    /**
     * @ORM\Column(type="date")
     */
    private $date;

    public function __construct(
        float $sum,
        Currency $currency,
        Type $type,
        int $user_id,
        string $tag,
        ?\DateTime $date = null,
        ?string $description = null
    ) {
        $this->sum = $sum;
        $this->currency = $currency;
        $this->type = $type;
        $this->user_id = $user_id;
        $this->tag = $tag;
        $this->date = $date ?? new \DateTime();
        $this->description = $description;

        $this->registerEvent(new CreditAdded($sum, $currency, $user_id, $this->date));
    }

    /**
     * @ORM\PreRemove
     */
    public function onRemove()
    {
        $this->registerEvent(new CreditDeleted($this->sum, $this->currency, $this->user_id, $this->date));
    }
}
