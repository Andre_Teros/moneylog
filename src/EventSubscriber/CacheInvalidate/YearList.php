<?php
declare(strict_types=1);

namespace App\EventSubscriber\CacheInvalidate;

use App\Event\CreditAdded;
use App\Event\CreditDeleted;
use App\Service\YearListService;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class YearList implements EventSubscriberInterface
{
    private $yearListService;

    public function __construct(YearListService $yearListService)
    {
        $this->yearListService = $yearListService;
    }

    public function onCreditAdd(CreditAdded $event): void
    {
        $userId = $event->getUserId();
        $date = $event->getDate();

        if (!$this->yearListService->exists($userId)) {
            return;
        }

        $year = $date->format('Y');
        if (!in_array($year, $this->yearListService->get($userId), true)) {
            $this->yearListService->invalidate($userId);
        }
    }

    public function onCreditDelete(CreditDeleted $event): void
    {
        $userId = $event->getUserId();

        if (!$this->yearListService->exists($event->getUserId())) {
            return;
        }

        $this->yearListService->invalidate($userId);
    }

    public static function getSubscribedEvents(): array
    {
        return [
            CreditAdded::class => 'onCreditAdd',
            CreditDeleted::class => 'onCreditDelete',
        ];
    }
}
