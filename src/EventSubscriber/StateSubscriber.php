<?php
declare(strict_types=1);

namespace App\EventSubscriber;

use App\Entity\State;
use App\Event\CreditAdded;
use App\Event\CreditDeleted;
use App\Event\DebitAdded;
use App\Event\DebitDeleted;
use App\Event\DebtAdded;
use App\Event\DebtRepaid;
use App\Event\StateEventInterface;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use App\Repository\StateRepository;

class StateSubscriber implements EventSubscriberInterface
{
    private $stateRepository;
    private $entityManager;

    public function __construct(StateRepository $stateRepository, EntityManagerInterface $entityManager)
    {
        $this->stateRepository = $stateRepository;
        $this->entityManager = $entityManager;
    }

    public function increaseState(StateEventInterface $event): void
    {
        $state = $this->stateRepository->getByUserAndCurrency($event->getUserId(), $event->getCurrency());
        $state->changeSum($event->getSum(), State::INCREASE);

        $this->entityManager->persist($state);
    }

    public function decreaseState(StateEventInterface $event): void
    {
        $state = $this->stateRepository->getByUserAndCurrency($event->getUserId(), $event->getCurrency());
        $state->changeSum($event->getSum(), State::DECREASE);

        $this->entityManager->persist($state);
    }

    public static function getSubscribedEvents(): array
    {
        return [
            CreditAdded::class => 'decreaseState',
            CreditDeleted::class => 'increaseState',
            DebitAdded::class => 'increaseState',
            DebitDeleted::class => 'decreaseState',
            DebtAdded::class => 'decreaseState',
            DebtRepaid::class => 'increaseState',
        ];
    }
}
