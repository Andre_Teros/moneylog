<?php declare(strict_types=1);

namespace Application\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20190504110620 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE debits (
                               id INT AUTO_INCREMENT NOT NULL,
                               currency_id INT NOT NULL,
                               type_id INT NOT NULL,
                               source_id INT NOT NULL,
                               sum NUMERIC(16, 3) NOT NULL,
                               user_id INT NOT NULL,
                               description LONGTEXT DEFAULT NULL,
                               date DATE NOT NULL,
                               created TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
                               updated TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
                               INDEX IDX_2703478138248176 (currency_id),
                               INDEX IDX_27034781C54C8C93 (type_id),
                               INDEX IDX_27034781953C1C61 (source_id),
                               PRIMARY KEY(id)
                            )
                            DEFAULT CHARACTER SET UTF8 COLLATE UTF8_unicode_ci ENGINE = InnoDB');

        $this->addSql("INSERT INTO types (name, belongs_to)
                           VALUES
                               ('gift', 1),
                               ('profit', 1);");
    }

    public function down(Schema $schema) : void
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE debits');
    }
}
