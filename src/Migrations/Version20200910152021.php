<?php
declare(strict_types=1);

namespace Application\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20200910152021 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE debts (
                               id INT AUTO_INCREMENT NOT NULL,
                               currency_id INT NOT NULL,
                               sum NUMERIC(16, 3) NOT NULL,
                               user_id INT NOT NULL,
                               debtor VARCHAR(255) NOT NULL,
                               repaid TINYINT(1) NOT NULL,
                               date DATE NOT NULL,
                               return_date DATE DEFAULT NULL,
                               INDEX IDX_6F64A29B38248176 (currency_id),
                               PRIMARY KEY(id)
                           ) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');

        $this->addSql('ALTER TABLE debts ADD CONSTRAINT FK_6F64A29B38248176 FOREIGN KEY (currency_id) REFERENCES currencies (id)');
    }

    public function down(Schema $schema) : void
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE debts');
    }
}
