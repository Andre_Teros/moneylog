<?php declare(strict_types=1);

namespace Application\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20190424071541 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE credits (
                               id INT AUTO_INCREMENT NOT NULL,
                               currency_id INT NOT NULL,
                               type_id INT NOT NULL,
                               sum NUMERIC(16, 3) NOT NULL,
                               user_id INT NOT NULL,
                               description LONGTEXT DEFAULT NULL,
                               date DATE NOT NULL,
                               created TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
                               updated TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
                               INDEX IDX_4117D17E38248176 (currency_id),
                               INDEX IDX_4117D17EC54C8C93 (type_id),
                               PRIMARY KEY(id)
                           )
                           DEFAULT CHARACTER SET UTF8 COLLATE UTF8_unicode_ci ENGINE = InnoDB');
    }

    public function down(Schema $schema) : void
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE credits');
    }
}
