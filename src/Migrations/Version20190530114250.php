<?php declare(strict_types=1);

namespace Application\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20190530114250 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE states (
                               id INT AUTO_INCREMENT NOT NULL,
                               currency_id INT NOT NULL,
                               sum NUMERIC(16, 3) NOT NULL,
                               user_id INT NOT NULL,
                               INDEX IDX_31C2774D38248176 (currency_id),
                               PRIMARY KEY(id)
                           )
                           DEFAULT CHARACTER SET UTF8 COLLATE UTF8_unicode_ci ENGINE = InnoDB');
    }

    public function down(Schema $schema) : void
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE states');
    }
}
