<?php declare(strict_types=1);

namespace Application\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;
use Symfony\Component\Dotenv\Dotenv;
use Symfony\Component\Security\Core\Encoder\BCryptPasswordEncoder;
use Symfony\Component\Yaml\Parser;

final class Version20190420091624 extends AbstractMigration
{
    private $paramsFile = __DIR__ . '/../config/parameters.yml';

    public function up(Schema $schema): void
    {
        $this->skipIf(!$this->executeForThisEnv(), 'execute only in dev environment');

        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $password = $this->getPassword();

        $this->addSql("INSERT INTO user (email, password, roles)
                           VALUES
                               ('admin@admin.com', '{$password}', '[\"ROLE_ADMIN\"]'),
                               ('user@user.com', '{$password}', '[\"ROLE_USER\"]');");
    }

    public function down(Schema $schema): void
    {
        $this->skipIf(!$this->executeForThisEnv(), 'execute only in dev environment');

        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql("DELETE FROM user
                           WHERE email IN ('admin@admin.com', 'user@user.com');");
    }

    private function getPassword(): string
    {
        $yamlParser = new Parser();
        $encoder = new BCryptPasswordEncoder(13);

        $parameters = $yamlParser->parseFile($this->paramsFile);
        return $encoder->encodePassword($parameters['parameters']['test_user_password'], '');

        return $parameters['parameters']['test_user_password'];
    }

    private function executeForThisEnv(): bool
    {
        (new Dotenv())->load(dirname(__DIR__).'/../.env');

        return !empty($_ENV['APP_ENV']) && $_ENV['APP_ENV'] === 'dev';
    }
}
