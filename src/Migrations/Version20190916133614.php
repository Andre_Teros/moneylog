<?php declare(strict_types=1);

namespace Application\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20190916133614 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE currencies ADD sign CHAR(1) NULL');
        $this->addSql('UPDATE currencies SET sign = "₴" WHERE name = "UAH"');
        $this->addSql('UPDATE currencies SET sign = "$" WHERE name = "USD"');
        $this->addSql('UPDATE currencies SET sign = "€" WHERE name = "EUR"');
    }

    public function down(Schema $schema) : void
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE currencies DROP sign');
    }
}
