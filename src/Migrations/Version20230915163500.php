<?php

declare(strict_types=1);

namespace Application\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20230915163500 extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'Alter currencies table';
    }

    public function up(Schema $schema) : void
    {
        $this->addSql('ALTER TABLE `currencies` CHANGE `sign` `sign` char(2) COLLATE \'utf8_unicode_ci\' NULL');
        $this->addSql('ALTER TABLE `currencies` ADD `order` TINYINT(1) UNSIGNED DEFAULT 255;');
    }

    public function down(Schema $schema) : void
    {
        $this->addSql('ALTER TABLE `currencies` CHANGE `sign` `sign` char(1) COLLATE \'utf8_unicode_ci\' NULL');
        $this->addSql('ALTER TABLE `currencies` DROP `order`;');
    }
}
