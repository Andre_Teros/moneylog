<?php declare(strict_types=1);

namespace Application\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20190512113755 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE capital (
                               id INT AUTO_INCREMENT NOT NULL,
                               sum NUMERIC(16, 3) NOT NULL,
                               currency_id INT NOT NULL,
                               capital_location_id INT NOT NULL,
                               user_id INT NOT NULL,
                               date DATE NOT NULL,
                               created TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
                               updated TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
                               INDEX IDX_307CBAA638248176 (currency_id),
                               INDEX IDX_307CBAA638248177 (capital_location_id),
                               PRIMARY KEY(id)
                            )
                            DEFAULT CHARACTER SET UTF8 COLLATE UTF8_unicode_ci ENGINE = InnoDB');

        $this->addSql('CREATE TABLE capital_locations (
                               id INT AUTO_INCREMENT NOT NULL,
                               name VARCHAR(255) NOT NULL,
                               PRIMARY KEY(id)
                           )
                           DEFAULT CHARACTER SET UTF8 COLLATE UTF8_unicode_ci ENGINE = InnoDB');
    }

    public function down(Schema $schema): void
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE capital');
        $this->addSql('DROP TABLE capital_locations');
    }
}
