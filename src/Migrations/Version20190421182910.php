<?php declare(strict_types=1);

namespace Application\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20190421182910 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE currencies (
                               id INT AUTO_INCREMENT NOT NULL,
                               name CHAR(3) NOT NULL,
                               UNIQUE INDEX (name),
                               PRIMARY KEY(id)
                           )
                           DEFAULT CHARACTER SET UTF8 COLLATE UTF8_unicode_ci ENGINE = InnoDB');

        $this->addSql("INSERT INTO currencies (name)
                           VALUES
                               ('UAH'),
                               ('USD'),
                               ('EUR');");
    }

    public function down(Schema $schema): void
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE currencies');
    }
}
