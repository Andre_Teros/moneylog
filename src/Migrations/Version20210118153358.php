<?php
declare(strict_types=1);

namespace Application\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20210118153358 extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'Create plans table';
    }

    public function up(Schema $schema) : void
    {
        $sql = '
            CREATE TABLE plans (
                year INT(4) NOT NULL,
                user_id INT NOT NULL,
                aim INT,
                month_aims JSON NOT NULL,
                PRIMARY KEY(user_id, year)
            )
            DEFAULT CHARACTER SET UTF8 COLLATE UTF8_unicode_ci ENGINE = InnoDB
        ';

        $this->addSql($sql);
    }

    public function down(Schema $schema) : void
    {
        $this->addSql('DROP TABLE plans');
    }
}
