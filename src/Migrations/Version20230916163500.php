<?php

declare(strict_types=1);

namespace Application\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20230916163500 extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'Add PLN currency and order';
    }

    public function up(Schema $schema) : void
    {
        $this->addSql("INSERT INTO `currencies` (`name`, `sign`, `order`) VALUES ('PLN', 'zł', 4)");
        $this->addSql("UPDATE `currencies` SET `order` = 1 WHERE `name` = 'UAH'");
        $this->addSql("UPDATE `currencies` SET `order` = 2 WHERE `name` = 'USD'");
        $this->addSql("UPDATE `currencies` SET `order` = 3 WHERE `name` = 'EUR'");

    }

    public function down(Schema $schema) : void
    {
        $this->addSql("DELETE FROM `currencies` WHERE name = 'PLN'");
    }
}
