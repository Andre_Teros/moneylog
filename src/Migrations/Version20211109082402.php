<?php

declare(strict_types=1);

namespace Application\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20211109082402 extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'Alter plans table';
    }

    public function up(Schema $schema) : void
    {
        $this->addSql('ALTER TABLE plans DROP COLUMN aim');
        $this->addSql('ALTER TABLE plans ADD overvalue INT NOT NULL DEFAULT 0');

    }

    public function down(Schema $schema) : void
    {
        $this->addSql('ALTER TABLE plans DROP COLUMN overvalue');
        $this->addSql('ALTER TABLE plans ADD aim INT');
    }
}
