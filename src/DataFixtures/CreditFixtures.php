<?php
declare(strict_types=1);

namespace App\DataFixtures;

use App\Entity\Credit;
use App\Entity\Currency;
use App\Entity\Type;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class CreditFixtures extends Fixture implements FixtureGroupInterface, DependentFixtureInterface
{
    /** @var ObjectManager */
    private $manager;

    public function load(ObjectManager $manager)
    {
        $this->manager = $manager;
        $startTime = new \DateTime('-2 months');
        $endTime = new \DateTime('+2 months');

        for ($i = 0; $i < 40; $i++) {
            $debit = new Credit(
                random_int(10, 30000),
                $this->getCurrency(),
                $this->getType(),
                $this->getUserId(),
                $this->getTag(),
                $this->getRandomDateInRange($startTime, $endTime)
            );
            $manager->persist($debit);
        }

        $manager->flush();
    }

    public function getDependencies()
    {
        return [
            SourceFixtures::class,
        ];
    }

    private function getCurrency()
    {
        $currId = (random_int(0, 9) < 6) ? 1 : random_int(2, 3);

        return $this->manager->getRepository(Currency::class)->find($currId);
    }

    private function getType()
    {
        $typeId = random_int(1, 2);

        return $this->manager->getRepository(Type::class)->find($typeId);
    }

    private function getUserId()
    {
        $user = $this->manager->getRepository(User::class)->findOneBy(['email' => 'user@user.com']);

        return $user->getId();
    }

    private function getTag()
    {
        $aTags = [
            'food',
            'clothes',
            'devices',
            'medical',
        ];
        $key = array_rand($aTags);

        return $aTags[$key];
    }

    private function getRandomDateInRange(\DateTime $start, \DateTime $end): \DateTime
    {
        $randomTimestamp = random_int($start->getTimestamp(), $end->getTimestamp());
        $randomDate = new \DateTime();
        $randomDate->setTimestamp($randomTimestamp);
        return $randomDate;
    }

    public static function getGroups(): array
    {
        return ['credit'];
    }
}
