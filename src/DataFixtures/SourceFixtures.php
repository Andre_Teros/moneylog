<?php
declare(strict_types=1);

namespace App\DataFixtures;

use App\Entity\Source;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use Doctrine\Common\Persistence\ObjectManager;

class SourceFixtures extends Fixture implements FixtureGroupInterface
{
    /** @var ObjectManager */
    private $manager;

    public function load(ObjectManager $manager)
    {
        $this->manager = $manager;

        for ($i = 0; $i < 3; $i++) {
            $source = new Source(
                $this->getName($i),
                $this->getUserId()
            );
            $manager->persist($source);
        }

        $manager->flush();
    }

    public function getUserId()
    {
        $user = $this->manager->getRepository(User::class)->findOneBy(['email' => 'user@user.com']);

        return $user->getId();
    }

    public function getName($i)
    {
        $aTags = [
            'work',
            'freelance',
            'someone',
        ];

        return $aTags[$i];
    }

    public static function getGroups(): array
    {
        return ['source'];
    }
}
