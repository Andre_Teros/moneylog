<?php
declare(strict_types=1);

namespace App\DataFixtures;

use App\Entity\Currency;
use App\Entity\State;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use Doctrine\Common\Persistence\ObjectManager;

class StateFixtures extends Fixture implements FixtureGroupInterface
{
    /** @var ObjectManager */
    private $manager;

    public function load(ObjectManager $manager)
    {
        $this->manager = $manager;

        $state = new State(
            1000,
            $this->getCurrency(),
            $this->getUserId()
        );

        $manager->persist($state);
        $manager->flush();
    }

    private function getCurrency()
    {
        $currId = (random_int(0, 9) < 6) ? 1 : random_int(2, 3);

        return $this->manager->getRepository(Currency::class)->find($currId);
    }

    private function getUserId()
    {
        $user = $this->manager->getRepository(User::class)->findOneBy(['email' => 'user@user.com']);

        return $user->getId();
    }

    public static function getGroups(): array
    {
        return ['state'];
    }
}
