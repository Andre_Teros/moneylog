<?php
declare(strict_types=1);

namespace App\Event;

use App\Entity\Currency;
use Symfony\Contracts\EventDispatcher\Event;

class DebtAdded extends Event implements StateEventInterface
{
    private $sum;
    private $currency;
    private $userId;

    public function __construct(
        float $sum,
        Currency $currency,
        int $userId
    ) {
        $this->sum = $sum;
        $this->currency = $currency;
        $this->userId = $userId;
    }

    public function getSum(): float
    {
        return $this->sum;
    }

    public function getCurrency(): Currency
    {
        return $this->currency;
    }

    public function getUserId(): int
    {
        return $this->userId;
    }
}
