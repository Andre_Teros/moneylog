<?php
declare(strict_types=1);

namespace App\Event;

use App\Entity\Currency;

interface StateEventInterface
{
    public function getSum(): float;

    public function getCurrency(): Currency;

    public function getUserId(): int;
}
