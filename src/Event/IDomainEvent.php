<?php
declare(strict_types=1);

namespace App\Event;

use Symfony\Contracts\EventDispatcher\Event;

interface IDomainEvent
{
    public function registerEvent(Event $event): void;

    /**
     * @return Event[]
     */
    public function popEvents(): array;
}
