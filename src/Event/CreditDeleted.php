<?php
declare(strict_types=1);

namespace App\Event;

use App\Entity\Currency;
use Symfony\Contracts\EventDispatcher\Event;

class CreditDeleted extends Event implements StateEventInterface
{
    private $sum;
    private $currency;
    private $userId;
    private $date;

    public function __construct(
        float $sum,
        Currency $currency,
        int $userId,
        \DateTime $date
    ) {
        $this->sum = $sum;
        $this->currency = $currency;
        $this->userId = $userId;
        $this->date = $date;
    }

    public function getSum(): float
    {
        return $this->sum;
    }

    public function getCurrency(): Currency
    {
        return $this->currency;
    }

    public function getUserId(): int
    {
        return $this->userId;
    }

    public function getDate(): \DateTime
    {
        return $this->date;
    }
}
