<?php
declare(strict_types=1);

namespace App\Service;

use App\DTO\DebitDTO;
use App\Entity\Debit;
use App\Entity\Source;
use App\Entity\Type;
use App\Exceptions\NoEntityFound;
use App\Repository\CurrencyRepository;
use App\Repository\SourceRepository;
use App\Repository\TypeRepository;
use App\Repository\DebitRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

class DebitService
{
    private $typeRepository;
    private $currencyRepository;
    private $sourceRepository;
    private $debitRepository;
    private $entityManager;
    private $eventDispatcher;

    public function __construct(
        TypeRepository $typeRepository,
        CurrencyRepository $currencyRepository,
        SourceRepository $sourceRepository,
        DebitRepository $debitRepository,
        EntityManagerInterface $entityManager,
        EventDispatcherInterface $eventDispatcher
    ) {
        $this->typeRepository = $typeRepository;
        $this->currencyRepository = $currencyRepository;
        $this->sourceRepository = $sourceRepository;
        $this->debitRepository = $debitRepository;
        $this->entityManager = $entityManager;
        $this->eventDispatcher = $eventDispatcher;
    }

    /**
     * @throws NoEntityFound
     */
    public function create(DebitDTO $dto, int $userId): void
    {
        if ($dto->group === DebitDTO::GROUP_ID) {
            $source = $this->sourceRepository->get((int)$dto->source);
        } else {
            // TODO: add unique validation
            $source = new Source($dto->source, $userId);
            $this->entityManager->persist($source);
        }

        $currency = $this->currencyRepository->get((int)$dto->currency);
        $type = $this->typeRepository->get((int)$dto->type, Type::BELONGS_TO_DEBIT);
        $date = $dto->date ? \DateTime::createFromFormat('Y-m-d', $dto->date) : null;

        $debit = new Debit(
            (float)$dto->sum,
            $currency,
            $type,
            $userId,
            $source,
            $date,
            $dto->description
        );

        foreach ($debit->popEvents() as $event) {
            $this->eventDispatcher->dispatch($event);
        }

        $this->entityManager->persist($debit);
    }

    /**
     * @throws NoEntityFound
     */
    public function delete(int $debitId): void
    {
        $debit = $this->debitRepository->get($debitId);
        $this->entityManager->remove($debit);

        foreach ($debit->popEvents() as $event) {
            $this->eventDispatcher->dispatch($event);
        }
    }
}
