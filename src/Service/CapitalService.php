<?php
declare(strict_types=1);

namespace App\Service;

use App\Entity\Capital;
use App\Repository\CapitalLocationRepository;
use App\Repository\CapitalRepository;
use App\Repository\CurrencyRepository;
use Doctrine\ORM\EntityManagerInterface;

class CapitalService
{
    private $currencyRepository;
    private $capitalRepository;
    private $capitalLocationRepository;
    private $entityManager;

    public function __construct(
        CurrencyRepository $currencyRepository,
        CapitalRepository $capitalRepository,
        CapitalLocationRepository $capitalLocationRepository,
        EntityManagerInterface $entityManager
    ) {
        $this->currencyRepository = $currencyRepository;
        $this->capitalRepository = $capitalRepository;
        $this->capitalLocationRepository = $capitalLocationRepository;
        $this->entityManager = $entityManager;
    }

    public function create(array $data, int $userId): void
    {
        foreach ($data as $currencyId => $dataByCurrency) {
            foreach ($dataByCurrency as $locationId => $sum) {
                // TODO: change to $sum !== 0.0
                if (!empty($sum)) {
                    $currency = $this->currencyRepository->find($currencyId);
                    $location = $this->capitalLocationRepository->find($locationId);
                    $capital = new Capital(
                        (float)$sum,
                        $currency,
                        $location,
                        $userId
                    );

                    $this->entityManager->persist($capital);
                }
            }
        }
    }

    // TODO: Locking Support
    public function update(array $data, int $userId): void
    {
        $currentState = $this->capitalRepository->findLastStateForUpdate($userId);

        $newData = [];
        foreach ($data as $currencyId => $dataByCurrency) {
            foreach ($dataByCurrency as $locationId => $dataByLocation) {
                if ($dataByLocation) {
                    $newData[] = [
                        'sum' => $dataByLocation,
                        'currency' => $currencyId,
                        'location' => $locationId,
                    ];
                }
            }
        }

        $dataToDelete = array_udiff($currentState, $newData, function ($a, $b) {
            $aHash = sha1($a['currency'] . $a['location']);
            $bHash = sha1($b['currency'] . $b['location']);

            return $aHash <=> $bHash;
        });

        $dataToInsert = array_udiff($newData, $currentState, function ($a, $b) {
            $aHash = sha1($a['currency'] . $a['location']);
            $bHash = sha1($b['currency'] . $b['location']);

            return $aHash <=> $bHash;
        });

        foreach ($dataToInsert as $key => $itemToInsert) {
            $currency = $this->currencyRepository->find($itemToInsert['currency']);
            $location = $this->capitalLocationRepository->find($itemToInsert['location']);
            $capital = new Capital(
                (float)$itemToInsert['sum'],
                $currency,
                $location,
                $userId
            );

            $this->entityManager->persist($capital);
            unset($newData[$key]);
        }

        $dataToUpdate = array_udiff($newData, $currentState, function ($a, $b) {
            $aHash = sha1($a['currency'] . $a['location'] . $a['sum']);
            $bHash = sha1($b['currency'] . $b['location'] . $b['sum']);

            return $aHash <=> $bHash;
        });

        foreach ($dataToDelete as $itemToDelete) {
            $capital = $this->capitalRepository->findTodayItem($userId, (int)$itemToDelete['currency'], (int)$itemToDelete['location']);

            if ($capital) {
                $this->entityManager->remove($capital);
            }
        }

        foreach ($dataToUpdate as $itemToUpdate) {
            $capital = $this->capitalRepository->findTodayItem($userId, $itemToUpdate['currency'], $itemToUpdate['location']);

            if ($capital) {
                $capital->setSum((float)$itemToUpdate['sum']);
            }
        }
    }
}
