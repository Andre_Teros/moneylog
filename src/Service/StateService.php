<?php
declare(strict_types=1);

namespace App\Service;

use App\Entity\State;
use App\Repository\CurrencyRepository;
use Doctrine\ORM\EntityManagerInterface;

class StateService
{
    private $entityManager;
    private $currencyRepository;

    public function __construct(EntityManagerInterface $entityManager, CurrencyRepository $currencyRepository)
    {
        $this->entityManager = $entityManager;

        $this->currencyRepository = $currencyRepository;
    }

    /**
     * @deprecated
     */
    public function increase(int $userId, int $currencyId, float $sum): void
    {
        /** @var State $state */
        $state = $this->entityManager->getRepository(State::class)->findOneBy([
            'user_id' => $userId,
            'currency' => $currencyId,
        ]);

        if ($state) {
            $state->changeSum($sum, State::INCREASE);
        } else {
            $currency = $this->currencyRepository->find($currencyId);
            $state = new State(
                $sum,
                $currency,
                $userId
            );
            $this->entityManager->persist($state);
        }
    }

    /**
     * @deprecated
     */
    public function decrease(int $userId, int $currencyId, float $sum): void
    {
        /** @var State $state */
        $state = $this->entityManager->getRepository(State::class)->findOneBy([
            'user_id' => $userId,
            'currency' => $currencyId,
        ]);

        if ($state) {
            $state->changeSum($sum, State::DECREASE);
        } else {
            $currency = $this->currencyRepository->find($currencyId);
            $state = new State(
                -$sum,
                $currency,
                $userId
            );
            $this->entityManager->persist($state);
        }
    }

}
