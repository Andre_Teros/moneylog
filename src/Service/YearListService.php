<?php
declare(strict_types=1);

namespace App\Service;

use App\Repository\CreditRepository;
use Doctrine\Common\Cache\Cache;

class YearListService
{
    public const CACHE_KEY = 'year_list_';

    private $creditRepository;
    private $cache;

    public function __construct(CreditRepository $creditRepository, Cache $cache)
    {
        $this->creditRepository = $creditRepository;
        $this->cache = $cache;
    }

    public function get(int $userId): array
    {
        $cacheKey = self::CACHE_KEY . $userId;

        if ($this->exists($userId)) {
            return $this->cache->fetch($cacheKey);
        }

        $years = $this->creditRepository->findYears($userId);
        $this->cache->save($cacheKey, $years);

        return $years;
    }

    public function exists(int $userId): bool
    {
        $cacheKey = self::CACHE_KEY . $userId;

        return $this->cache->contains($cacheKey);
    }

    public function invalidate(int $userId): void
    {
        $cacheKey = self::CACHE_KEY . $userId;

        $this->cache->delete($cacheKey);
    }
}
