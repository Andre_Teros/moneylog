<?php
declare(strict_types=1);

namespace App\Service;

use App\Repository\PlanRepository;
use Doctrine\DBAL\DBALException;

class PlanService
{
    public const DEFAULT_RESULT = [
        'aim' => 0,
        'month_aims' => '{}',
    ];

    private $repository;

    public function __construct(PlanRepository $repository)
    {
        $this->repository = $repository;
    }

    public function getPlan(int $userId): array
    {
        $currentYear = (int)date('Y');
        $allPlansData = $this->repository->getAllPlans($userId);

        if (empty($allPlansData)) {
            $allPlansData = self::DEFAULT_RESULT;
            $allPlansData['year'] = $currentYear;
        }

        foreach ($allPlansData as $key => $yearData) {
            $allPlansData[$key]['month_aims'] = json_decode($yearData['month_aims'], true);
            $allPlansData[$key]['overvalue'] = (float) $yearData['overvalue'];
            $allPlansData[$key]['year'] = (int) $yearData['year'];
        }

        return [
            'allPlansData' => $allPlansData,
            'chosenYear' => (int)date('Y'),
        ];
    }

    public function savePlan(array $data, int $userId, ?int $year = null): bool
    {
        $year = $year ?? (int)date('Y');

        $exist = $this->repository->find($userId, $year);
        $overvalue = $data['overvalue'] ?? null;
        $monthAims = !empty($data['monthAims'])
            ? json_encode($data['monthAims'])
            : null;

        try {
            if ($exist === false) {
                return  $this->repository->insert($userId, $year, $overvalue, $monthAims);
            }

            return  $this->repository->update($userId, $year, $overvalue, $monthAims);
        } catch (DBALException $e) {
            // todo: log here
        }

        return false;
    }
}
