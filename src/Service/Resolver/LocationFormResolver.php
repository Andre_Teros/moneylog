<?php
declare(strict_types=1);

namespace App\Service\Resolver;

use App\DTO\CapitalLocationDTO;
use App\Service\Validator\Csrf;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Controller\ArgumentValueResolverInterface;
use Symfony\Component\HttpKernel\ControllerMetadata\ArgumentMetadata;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;

class LocationFormResolver implements ArgumentValueResolverInterface
{
    private $csrfValidator;
    private $denormalizer;

    public function __construct(Csrf $csrfValidator, DenormalizerInterface $denormalizer)
    {
        $this->csrfValidator = $csrfValidator;
        $this->denormalizer = $denormalizer;
    }

    public function supports(Request $request, ArgumentMetadata $argument): bool
    {
        return $argument->getType() === CapitalLocationDTO::class;
    }

    public function resolve(Request $request, ArgumentMetadata $argument): \Generator
    {
        //TODO: activate
//        $this->csrfValidator->validate('location_form', $request->request->get('_csrf_token'));

        $data = $request->request->get('location');
        yield $this->denormalizer->denormalize($data, $argument->getType());
    }
}
