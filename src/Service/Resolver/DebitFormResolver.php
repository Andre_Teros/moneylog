<?php
declare(strict_types=1);

namespace App\Service\Resolver;

use App\DTO\DebitDTO;
use App\Exceptions\WrongValidation;
use App\Service\Validator\Csrf;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Controller\ArgumentValueResolverInterface;
use Symfony\Component\HttpKernel\ControllerMetadata\ArgumentMetadata;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class DebitFormResolver implements ArgumentValueResolverInterface
{
    public const ID = 'debit';

    private $csrfValidator;
    private $validator;

    public function __construct(Csrf $csrfValidator, ValidatorInterface $validator)
    {
        $this->csrfValidator = $csrfValidator;
        $this->validator = $validator;
    }

    public function supports(Request $request, ArgumentMetadata $argument): bool
    {
        return $argument->getType() === DebitDTO::class;
    }

    public function resolve(Request $request, ArgumentMetadata $argument): \Generator
    {
        $this->csrfValidator->validate('debit_form', $request->request->get('_csrf_token'));

        $data = $request->request->get('debit');

        $dto = DebitDTO::fromRequest($data);

        $validateGroup = $this->isSourceId($dto) ? DebitDTO::GROUP_ID : DebitDTO::GROUP_NAME;
        $dto->group = $validateGroup;
        $this->validate($dto, $validateGroup);

        yield $dto;
    }

    private function validate(DebitDTO $dto, string $validateGroup)
    {
        $errors = $this->validator->validate($dto, null, [Constraint::DEFAULT_GROUP, $validateGroup]);
        if ($errors->count() !== 0) {
            throw new WrongValidation(self::ID, $errors);
        }
    }

    private function isSourceId($dto): bool
    {
        $errors = $this->validator->validate($dto, null, ['id']);

        return $errors->count() === 0;
    }
}
