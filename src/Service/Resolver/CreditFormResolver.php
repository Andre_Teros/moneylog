<?php
declare(strict_types=1);

namespace App\Service\Resolver;

use App\DTO\CreditDTO;
use App\Exceptions\WrongValidation;
use App\Service\Validator\Csrf;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Controller\ArgumentValueResolverInterface;
use Symfony\Component\HttpKernel\ControllerMetadata\ArgumentMetadata;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class CreditFormResolver implements ArgumentValueResolverInterface
{
    public const ID = 'credit';

    private $csrfValidator;
    private $validator;

    public function __construct(Csrf $csrfValidator, ValidatorInterface $validator)
    {
        $this->csrfValidator = $csrfValidator;
        $this->validator = $validator;
    }

    public function supports(Request $request, ArgumentMetadata $argument): bool
    {
        return $argument->getType() === CreditDTO::class;
    }

    public function resolve(Request $request, ArgumentMetadata $argument): \Generator
    {
        $this->csrfValidator->validate('credit_form', $request->request->get('_csrf_token'));

        $data = $request->request->get('credit');

        $dto = CreditDTO::fromRequest($data);

        $this->validate($dto);

        yield $dto;
    }

    private function validate($dto)
    {
        $errors = $this->validator->validate($dto);

        if ($errors->count() !== 0) {
            throw new WrongValidation(self::ID, $errors);
        }
    }
}
