<?php
declare(strict_types=1);

namespace App\Service\Resolver;

use App\DTO\DebtDTO;
use App\Exceptions\WrongValidation;
use App\Service\Validator\Csrf;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Controller\ArgumentValueResolverInterface;
use Symfony\Component\HttpKernel\ControllerMetadata\ArgumentMetadata;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class DebtFormResolver implements ArgumentValueResolverInterface
{
    public const ID = 'debt';

    private $csrfValidator;
    private $validator;

    public function __construct(Csrf $csrfValidator, ValidatorInterface $validator)
    {
        $this->csrfValidator = $csrfValidator;
        $this->validator = $validator;
    }

    public function supports(Request $request, ArgumentMetadata $argument): bool
    {
        return $argument->getType() === DebtDTO::class;
    }

    public function resolve(Request $request, ArgumentMetadata $argument): \Generator
    {
        $this->csrfValidator->validate('debt_form', $request->request->get('_csrf_token'));

        $data = $request->request->get('debt');

        $dto = DebtDTO::fromRequest($data);

        $this->validate($dto);

        yield $dto;
    }

    private function validate(DebtDTO $dto)
    {
        $errors = $this->validator->validate($dto);
        if ($errors->count() !== 0) {
            throw new WrongValidation(self::ID, $errors);
        }
    }
}
