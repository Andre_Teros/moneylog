<?php
declare(strict_types=1);

namespace App\Service;

use App\DTO\CreditDTO;
use App\Entity\Credit;
use App\Entity\Type;
use App\Exceptions\NoEntityFound;
use App\Repository\CreditRepository;
use App\Repository\CurrencyRepository;
use App\Repository\TypeRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

class CreditService
{
    private $typeRepository;
    private $currencyRepository;
    private $creditRepository;
    private $entityManager;
    private $eventDispatcher;

    public function __construct(
        TypeRepository $typeRepository,
        CurrencyRepository $currencyRepository,
        CreditRepository $creditRepository,
        EntityManagerInterface $entityManager,
        EventDispatcherInterface $eventDispatcher
    ) {
        $this->typeRepository = $typeRepository;
        $this->currencyRepository = $currencyRepository;
        $this->creditRepository = $creditRepository;
        $this->entityManager = $entityManager;
        $this->eventDispatcher = $eventDispatcher;
    }

    /**
     * @throws NoEntityFound
     */
    public function create(CreditDTO $dto, int $userId): void
    {
        $currency = $this->currencyRepository->get((int)$dto->currency);
        $type = $this->typeRepository->get((int)$dto->type, Type::BELONGS_TO_CREDIT);
        $date = $dto->date ? \DateTime::createFromFormat('Y-m-d', $dto->date) : null;

        $credit = new Credit(
            (float)$dto->sum,
            $currency,
            $type,
            $userId,
            $dto->tag,
            $date,
            $dto->description
        );

        foreach ($credit->popEvents() as $event) {
            $this->eventDispatcher->dispatch($event);
        }

        $this->entityManager->persist($credit);
    }

    /**
     * @throws NoEntityFound
     */
    public function delete(int $creditId): void
    {
        $credit = $this->creditRepository->get($creditId);
        $this->entityManager->remove($credit);

        foreach ($credit->popEvents() as $event) {
            $this->eventDispatcher->dispatch($event);
        }
    }
}
