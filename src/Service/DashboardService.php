<?php
declare(strict_types=1);

namespace App\Service;

use App\Repository\CapitalLocationRepository;
use App\Repository\CapitalRepository;
use App\Repository\CreditRepository;
use App\Repository\CurrencyRepository;
use App\Repository\DebitRepository;
use App\Repository\StateRepository;
use App\Templating\Capital\IDataAggregator;
use App\Templating\Credit\IDataTransformer;

class DashboardService
{
    private $capitalRepository;
    private $capitalLocationRepository;
    private $dataAggregator;
    private $stateRepository;
    private $debitRepository;
    private $creditRepository;
    private $currencyRepository;
    private $dataTransformer;

    private $capitalLocationData;

    public function __construct(
        CapitalRepository $capitalRepository,
        CapitalLocationRepository $capitalLocationRepository,
        IDataAggregator $dataAggregator,
        StateRepository $stateRepository,
        DebitRepository $debitRepository,
        CreditRepository $creditRepository,
        CurrencyRepository $currencyRepository,
        IDataTransformer $dataTransformer
    ){
        $this->capitalRepository = $capitalRepository;
        $this->capitalLocationRepository = $capitalLocationRepository;
        $this->dataAggregator = $dataAggregator;
        $this->stateRepository = $stateRepository;
        $this->debitRepository = $debitRepository;
        $this->creditRepository = $creditRepository;
        $this->currencyRepository = $currencyRepository;
        $this->dataTransformer = $dataTransformer;
    }

    public function getCapitalLocationData(int $userId): array
    {
        if ($this->capitalLocationData === null) {
            $this->capitalLocationData = $this->capitalLocationRepository->getAll($userId, ['name']);
        }

        return $this->capitalLocationData;
    }

    public function getCapitalData(int $userId): array
    {
        $rawCapitalData = $this->capitalRepository->findTop($userId, 5);
        $locationData = $this->getCapitalLocationData($userId);

        if (empty($rawCapitalData)) {
            return [];
        }

        return $this->dataAggregator->groupCapitalData($rawCapitalData, $locationData);
    }

    public function getGroupedCapitalData(int $userId): array
    {
        return $this->capitalRepository->findLastStateByCurrencies($userId);
    }

    public function getStateData(int $userId): array
    {
        return $this->stateRepository->findByUser($userId);
    }

    public function getDebitData(int $userId): array
    {
        return $this->debitRepository->findMonthly($userId);
    }

    public function getCreditData(int $userId): array
    {
        $rawData = $this->creditRepository->findSummaryCurrentMonth($userId);
        $currencyData = $this->currencyRepository->getAll(['name', 'order']);

        return $this->dataTransformer->transform($rawData, $currencyData);
    }
}
