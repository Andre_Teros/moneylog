<?php
declare(strict_types=1);

namespace App\Service\Validator;

use Symfony\Component\Security\Core\Exception\InvalidCsrfTokenException;
use Symfony\Component\Security\Csrf\CsrfToken;
use Symfony\Component\Security\Csrf\CsrfTokenManagerInterface;

class Csrf
{
    public function __construct(CsrfTokenManagerInterface $tokenManager)
    {
        $this->tokenManager = $tokenManager;
    }

    public function validate($id, $value): void
    {
        if (!$this->tokenManager->isTokenValid(new CsrfToken($id, $value))) {
            // TODO: add subscriber
            throw new InvalidCsrfTokenException();
        }
    }
}
