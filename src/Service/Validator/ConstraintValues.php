<?php
declare(strict_types=1);

namespace App\Service\Validator;

class ConstraintValues
{
    public const WORD = '/^[a-zA-Zа-яА-ЯіІїЇєЄёЁ][a-zA-Zа-яА-ЯіІїЇєЄёЁ0-9-+_ ]*$/u';

    public const ID = '/^\d+$/';

    public const FLOAT = '/^\d+(\.\d{1,3})?$/';
}
