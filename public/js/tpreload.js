(function (window) {
    window.TPreLoad = function (options) {
        this.options = (typeof options === "object") ? options : {};
    };

    window.TPreLoad.prototype.turnOn = function (global, selector, id) {
        var div = document.createElement('div');
        if (id !== undefined) {
            div.setAttribute('id', id);
        }
        div.setAttribute('class', 'preloaderblock');
        div.style.zIndex = this.options.zIndex !== undefined ? this.options.zIndex : '100';
        div.style.backgroundImage = this.options.backgroundImage !== undefined ? "url('" + this.options.backgroundImage + "')" : "";
        if (global === true) {
            div.style.position = "fixed";
            document.getElementsByTagName('body')[0].appendChild(div);
        } else if (typeof selector === "string") {
            div.style.position = "absolute";
            var container = document.querySelector(selector);
            container.style.position = "relative";
            container.appendChild(div);
        } else if (selector instanceof HTMLElement) {
            div.style.position = "absolute";
            selector.style.position = "relative";
            selector.appendChild(div);
        }
    };

    window.TPreLoad.prototype.turnOff = function (id) {
        if (id !== undefined) {
            if (document.getElementById(id)) {
                document.getElementById(id).remove();
            }
        } else {
            var preloaders = document.getElementsByClassName('preloaderblock');

            for (var i = 0; i < preloaders.length; i++) {
                preloaders[i].remove();
            }
        }
    };

})(window);
