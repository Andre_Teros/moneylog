moneylog
========

docker volume create --name=money_data
docker-compose up -d

docker-compose run --rm php sh -lc 'COMPOSER_MEMORY_LIMIT=-1 composer install'

docker-compose exec -T php sh -lc './bin/console d:d:c --if-not-exists'
docker-compose exec -T php sh -lc './bin/console d:m:m -n'
docker-compose exec -T php sh -lc './bin/console d:f:l -n --append'
